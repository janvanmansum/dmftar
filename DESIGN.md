dmftar DESIGN
=============

dmftar is a DMF-aware archiving tool. Under the hood it relies on GNU tar.
To work with potentially large datasets (hundreds of terabytes) archives
are split into multiple volumes. This splitting is performed by tar.
The files to pack and the volumes themselves are staged in DMF as needed.
dmftar makes batches or series of prefetches to do this; not all data
will be brought online at once, because some datasets will be too large
to handle all at once.

dmftar can work with archives stored locally on a system, or on a remote
server. For example, a user on the supercomputer can archive her dataset
and immediately store it in the Data Archive. dmftar will upload and
store the volumes one by one as the archive is being created, at no time
there shall be a copy of the entire archive present on the supercomputer.
This aspect is important for large datasets where you would not have
twice the storage space available.
Similarly, a user could have a laptop with limited storage space and
still be able to send data in and out the Data Archive.

Likewise it is possible to retrieve and extract an archive from a remote
server; dmftar downloads and extracts volume by volume, requiring less
total space. Meanwhile, the server prefetch-stages volumes in DMF to
keep the stream flowing.


Terminology
-----------

* dataset : user collection of files
  In general we assume a dataset resides under a single project directory
* archive : packed dataset, to be stored in a storage system for
  long term archiving
* volume : part of an archive; archives are split into multiple volumes.
  A volume is a single tar file with a sequence number
* member : a single file that is present in the archive
* remote archive : archive stored on a remote server
  A remote archive has an address like user@server:path/name.dmftar
  In practice, the server usually is archive.surfsara.nl


Functionality
-------------

Actions available for both local and remote archives:

	create			: create archive from directory
	list			: list archive content
	extract			: extract archive to local filesystem
	verify			: verify archive checksums
	regen-index		: recreate index files
	regen-checksum	: recreate checksum files
	delete-archive	: delete archive (volumes are marked as read-only)

dmftar functionality available only for local archives:

	verify-content	: check archive content against local directory tree
	interactive		: interactively browse file tree like in a UNIX shell
					  mark files for extraction, and extract

dmftar functionality available only for remote archives:

	download		: download remote archive


dmftar options:

	force-local		: not a remote address; for archive filenames that
					contain colon ':'
	keep-cache		: do not delete downloaded volumes (for remote archives)
	no-checksum		: skip checksumming
	files-from		: extract members given in file
	files-from0		: extract null-terminated members given in file

and the usual verbose/quiet/debug options and `--version` command.


dmftar parameters:

	config file		: defines bunch of parameters
	volume_size		: size of a single volume file (tar "tape-length")
	volbatch_size	: size of batch of files to bring online
	algorithm		: checksum algorithm (default: MD5)
	tar_binary		: path to GNU tar executable
	tar_options		: alternate tar options to include
	cmd_rsh			: ssh command used for remote server
	cmd_rcp			: scp command used for data transfer
	cmd_dmget		: dmget command used to bring online data
	cmd_dmput		: dmput command used to store data
	pipe_cmd_dmget_batch : command used to dmget batches of files
	cmd_find_batch	: command used to create batches


The dmftar-volume-changer uses the following environment variables:

	TAR_SUBCOMMAND	: set by `tar`; will be `-c`, `-t`, `-x`
	TAR_ARCHIVE		: name of current volume
	TAR_VOLUME		: number of next volume
	TAR_FD			: file descriptor to write name of new volume to
	DMFTAR_TMPFILE	: file with parameters and temp variables to be passed
					to volume-changer
	DMFTAR_NUMVOLS	: total number of volumes in archive
	DMFTAR_CACHEDIR : path where to store local cache copy of remote volume



Archives and volumes
--------------------

dmftar stores archives in a directory <name>.dmftar/
The volumes will be placed under a numbered directory: <name>.dmftar/0000/
There will be 1000 volumes per numbered directory at maximum.
With a default volume size of 10 GB, a single numbered subdir already
adds up to 10 TB. There is no limit in place for a maximum volume count;
the numbered subdir just goes up.

For data safety reasons all volumes are checksummed by default with MD5.
The checksums are stored in a small file alongside the volume file.
For data safety reasons completed archives are marked read-only. Because
the directories are read-only, the files can not easily inadvertedly be
deleted.

Like tape, `tar` files are a sequential medium and searching through them
is slow. Moreover the volume files are migrated offline by DMF. Therefore
to make the packed filelist searchable, index files are created, one for
each volume. The index is simply a `tar tf` output, listing all members.



dmftar-volume-changer
---------------------

GNU `tar` supports a volume-changer script so that you may insert a new tape
during the creation of multi-volume archives. We use this script hook to
have DMF stage the next batch of files, and to transfer remote volumes.

The flow of this process is that `dmftar` invokes `tar`, and every now
and then `tar` will invoke `dmftar-volume-changer`. The volume-changer runs
before the next volume, but after the current volume was finished. `tar`
will not run the volume-changer after the last volume was finished, so
often `dmftar` has to do some finishing work after `tar` exited.

The mindbender in this process is that `dmftar` has an instance of Archive
that has a Volume, and `dmftar-volume-changer` also has an instance of
Archive with a Volume. [Is this the current or next Volume?]

Some actions do not need `tar`, so then the volume-changer is not used
(for example, the `--download` function).



dmftar-server
-------------

The dmftar-server is a server-side helper that is run to manage
remote archives. It is not a server that runs like a daemon; it is started
via ssh and runs as a regular program in the context of the user.
`dmftar` gives it commands, for example to stage a list of volumes.


dmftar-server commands:

	help						Display this information
	list archive				List volumes in archive
	list0 archive				List volumes in machine-readable format
	exists archive				Check if archive directory exists
	count archive				Get volume count of archive
	index volume				Display volume index file
	chksum volume				Display volume checksum file
	size volume					Query volume file size
	mkdir archive				Make archive directory
	prefetch start+num+archive	Pre-stage volumes
	dmget volume.tar			Request get volume from tape
	dmput volume.tar			Request put volume on tape
	lock directory				Set volume directory as read-only
	unlock directory			Set volume directory write permission
	delete archive				Delete archive (no questions asked)
	version						Display version number
	quit						Exit program



Interactive mode
----------------

Interactive mode allows the user to interactively browse the content of
a `dmftar` archive using a shell-like command prompt, almost like a
FUSE-mount of the archive. It is not a FUSE-mount however, it's implemented
by reading the index files. It works a lot like a filesystem where a
directory node knows where to look for its directory listing
(number of the index file, plus the offset in the index file where that
directory listing begins) and uses that to display the directory listing
to the user. A paging and caching mechanism ensures that directories
feel responsive; giant directory trees do not need to be loaded entirely
before beginning browsing, moreover giant directory trees potentially
consume huge amounts of memory because of Python objects, which are quite
memory-hungry.

Interactive mode commands:

	help			Show this information
	ls				Show directory listing
	cd				Change directory
	pwd				Show current directory
	add				Add entries to extraction list
	remove			Remove entries from extraction list
	show			Display the extraction list
	extract			Extract selected files and directories
	exit, quit		Exit the dmftar shell

dmftar shell can pipe command output, for example:

	ls -l |more


The interactive mode code uses DirEntry instances, which are like vnodes.
These are cached in a DCACHE (for directories) and FCACHE (for file entries).
The cache expires in an LRU fashion.

+ listdir_archive(archive, entry) returns a list of DirEntry objects
+ find_dentry(archive, parent, name) returns DirEntry of name under parent dir
+ resolv_path(archive, path) return a DirEntry for given path



Classes
-------

Not all members and methods are listed, but this should give a general
overview.

Config:			dmftar/config.py
+ all parameters listed above
+ also some temp variables that probably don't really belong here
+ load()		loads config file
+ load_params()	calls load()
+ set()			set parameter (or temp variable)
+ getenv()		load params from $DMFTAR_TMPFILE
+ putenv()		write params to tmpfile and set $DMFTAR_TMPFILE
+ cleanup()		delete tmpfile
+ require_gnu_tar()	checks that we have GNU `tar`


Archive:		dmftar/archive.py
+ config				reference to loaded Config
+ path					path to archive
+ name					basename of archive
+ remote				remote address of archive (if it is a remote archive)
+ cache_dir             path of local cache dir (if it is a remote archive)
						eg. 'dmftar-cache.$pid'
+ tar_subcommand		basically denotes create/list/extract function
						can also be verify/download/whatnot
+ num_volumes			total number of volumes in archive
+ __str__()				returns path to archive
+ init_from_env(TarEnv)		initialize from a `TarEnv` instance
							also reads $DMFTAR_NUMVOLS
* volume_bynumber(n)	returns `Volume` or `RemoteVolume` instance
* count_volumes()		inits `Archive.num_volumes`
* _remote_count()
* _local_count()
* create()				calls create() in dmftar/create.py
* do_list()				calls do_list() in dmftar/index.py
* extract()				calls extract() in dmftar/extract.py
* verify()				calls verify() in dmftar/verify.py
* regenerate()			calls regenerate() in dmftar/regen.py
* delete()				calls delete() in dmftar/delete.py
* cleanup_environment()	deletes $TAR_xxx environment vars
* make_volume_batch()	make a list of volumes for dmget
* dmget_volume_batch()	issues xargs dmget for list of volumes
* cleanup_cachedir()    remove/delete local cache dir


Volume:			dmftar/volume.py
+ archive				reference to current Archive
+ number				volume number
+ __str__()				returns path + filename of volume
						eg. '$archive.path/0000/$archive.name-$volnum'
+ getsize()				returns filesize
+ stage()				bring volume online. Also prefetch next volumes,
						and verify the checksum
+ store()				store volume with dmput; also mark read-only,
						create index and checksum files
+ dmget()
+ dmput()
+ dmget_idx()			get index only
+ prefetch()			get next bunch of volumes
+ cleanup_cache()		no-op for local volumes
+ create_index()
+ create_checksum()
+ verify_checksum()
+ make_readonly()
+ make_index_readonly()
+ make_checksum_readonly()
+ recreate_index()
+ make_index_writeable()
+ recreate_chksum()
+ make_checksum_writeable()
+ lock_directory()		make directory read-only
+ lock_previous_directory()
+ unlock_directory()


RemoteVolume(Volume):	dmftar/remotevolume.py
+ cached			remotevolume content is on local disk
* __str__()			local (cached) path of volume file
					eg. '$cache_dir/$archive.name/0000/$archive.name-$volnum'
* cache_filename()	same as __str__()
* remote_filename()	remote path of volume file on the serverside
					eg. '$archive.path/0000/$archive.name-$volnum'
* getsize()			gets volume size from the server
* stage()			copy file from server to local
					This also does remote pre-staging
* store()			copy file to server and issue remote dmput
* verify_chksum()	verify volume against .chksum file
* cleanup_cache()	delete cached files
* lock_directory()		mark remote volume read-only
* unlock_directory()	mark remote dir read-write


TarEnv:			dmftar/tar_env.py
+ tar_subcommand	reflects $TAR_SUBCOMMAND
* tar_archive		reflects $TAR_ARCHIVE
* tar_volume		reflects $TAR_VOLUME
* tar_fd			reflects $TAR_FD
* getenv()			get $TAR_xxx environment variables
* check()			verify values
* putenv()			put $TAR_xxx environment variables


Batch:			dmftar/batch.py
+ path			temp directory
+ config		reference to Config
+ volnum		current volume we're working on
+ size			size of current batch
+ filelist[]	list of files in this batch
* make()		make set of batch files
* sync()		write filelist to a 'batch' file
* dmget()		issue dmget to bring online batch
* cleanup()		remove temp file + dir


IndexEntry:		dmftar/index.py
+ mode				unix permission bits as string
+ owner				username string
+ group				groupname string
+ size				filesize
+ mtime				unix time
+ path				full path as it appears in a tar archive
+ linkdest			symbolic link destination (often: None)
+ name				basename of path
+ imode				file permissions as integer
+ ifmt				file type as integer
* isdir()
* isfile()
* islink()
* ishardlink()
* parse()			parse line from index file into this IndexEntry


Checksum:		dmftar/checksum.py
+ filename
+ algorithm
+ sum				hex digest
* __eq__()			compare two checksums
* __ne__()			compare two checksums
* calc()			calculate checksum
* load()			read .chksum file
* save()			write .chksum file


ChecksumError(Exception):	dmftar/checksum.py


SummerInterface(ABC):		dmftar/checksum.py
* calc()			calculate checksum for file


Summer(SummerInterface)		dmftar/checksum.py
+ algorithm
+ new_func			function pointer; returns new hashlib-API-like
					instance
* calc()			calculate checksum for file


ExternalSummer(SummerInterface)		dmftar/checksum.py
+ algorithm
+ cmd 				external command name
* calc()			calculate checksum for file


DirEntry(IndexEntry):		dmftar/interactive.py

It's like a vnode for the in-memory directory tree for an archive index.


Completer:		dmftar/interactive.py

Tab-completer needed for GNU readline,
used by interactive mode prompt.


LRUCache:		dmftar/lrucache.py

Fixed size key-value store that expires oldest entry when full.
Used by interactive mode to cache DirEntry nodes.




ServerCommand:	dmftar-server.py
+ cmd			string
+ func			reference to command function
+ help_text		string
* invoke(*args)	run command function with arguments





EOB
