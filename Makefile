#
#	dmftar Makefile
#

buildroot = $(DESTDIR)
prefix = /usr
bindir = $(prefix)/bin
venvdir = /tmp/build-dmftar-venv

all:
	: # do nothing

install:
	if [ -d $(venvdir) ]; then rm -rf $(venvdir); fi
	python3 -m venv $(venvdir)
	. $(venvdir)/bin/activate ; \
	pip install -U pip ; \
	pip install -r requirements.txt ; \
	flit build && FLIT_ROOT_INSTALL=1 flit install ; \
	pip install --root $(buildroot) .
	#deactivate
	# virtualenv pip install typically adds the "virtual root" to the paths
	# therefore move the files/subdirs to the base buildroot dir
	# and patch the scripts to contain the correct path
	if [ -e $(buildroot)$(venvdir) ]; then \
		mkdir $(buildroot)$(prefix) ; \
		mv $(buildroot)$(venvdir)/etc $(buildroot)/ ; \
		mv $(buildroot)$(venvdir)/* $(buildroot)$(prefix)/ ; \
		mkdir -p $(buildroot)$(prefix)/lib/python3/dist-packages ; \
		mv $(buildroot)$(prefix)/lib/python3.*/site-packages/dmftar $(buildroot)$(prefix)/lib/python3/dist-packages/ ; \
		mv $(buildroot)$(prefix)/lib/python3.*/site-packages/dmftar-*.dist-info $(buildroot)$(prefix)/lib/python3/dist-packages/ ; \
		rmdir $(buildroot)$(prefix)/lib/python3.*/site-packages $(buildroot)$(prefix)/lib/python3.* ; \
		rmdir $(buildroot)$(venvdir) $(buildroot)/tmp ; \
		sed -i "1 s?$(venvdir)/?$(prefix)/?" $(buildroot)$(bindir)/dmftar ; \
		sed -i "1 s?$(venvdir)/?$(prefix)/?" $(buildroot)$(bindir)/dmftar-server ; \
		sed -i "1 s?$(venvdir)/?$(prefix)/?" $(buildroot)$(bindir)/dmftar-volume-changer ; \
	fi

clean:
	rm -rf $(venvdir)

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(bindir)/dmftar
	-rm -f $(DESTDIR)$(bindir)/dmftar-server
	-rm -f $(DESTDIR)$(bindir)/dmftar-volume-changer
	-rm -f $(DESTDIR)/etc/dmftar.conf
	-rm -rf $(DESTDIR)$(prefix)/lib/python*/site-packages/dmftar
	-rm -rf $(DESTDIR)$(prefix)/lib/python*/site-packages/dmftar-*.dist-info
	-rm -f $(DESTDIR)$(prefix)/share/man/man1/dmftar.1.gz

.PHONY: all install clean distclean uninstall

# EOB
