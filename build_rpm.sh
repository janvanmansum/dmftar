#! /bin/bash
#
#	build_rpm.sh
#

if [[ -z $1 ]]
then
	echo "usage: build_rpm.sh <release-tag>"
	exit 1
fi
RELEASE_TAG=$1

if [[ ! -e dmftar.1.ronn ]]
then
	echo "error: please chdir to the source directory"
	exit 1
fi

USER_NAME=$(git config user.name)
if [ -z "$USER_NAME" ]
then
	USER_NAME=$(whoami)
fi
USER_EMAIL=$(git config user.email)
if [ -z "$USER_EMAIL" ]
then
	USER_EMAIL="${USER_NAME}@$(hostname)"
fi

# get the python package name and python interpreter command
# suitable for this platform
PYTHON_PKG=$(./build_pythonversion.py |awk '{ print $1 }')
PYTHON_CMD=$(./build_pythonversion.py |awk '{ print $2 }')
if [ $? -ne 0 ]
then
	exit 1
fi

#
#	CentOS/RHEL packages are named with "el7" or "el8"
#	SLES packages get no special identifier
#
DIST=""
if [ -f /etc/os-release ]
then
	. /etc/os-release
	if [[ $ID == "centos" || $ID == "rhel" || $ID_LIKE == "rhel fedora" || $ID_LIKE == "fedora" ]]
	then
		DIST="el${VERSION_ID}."
	fi
fi

VERSION=$(echo $RELEASE_TAG | awk -F- '{ print $2 }')
RELEASE=$(echo $RELEASE_TAG | awk -F- -v dist="$DIST" '{ if ($3) { print dist $3 } else { print dist 1 } }')
rpmbuild -ba dmftar.spec \
	--define "tag_version $VERSION" \
	--define "tag_release $RELEASE" \
	--define "project_url https://gitlab.com/surfsara/dmftar/" \
	--define "user_name $USER_NAME" --define "user_email $USER_EMAIL" \
	--define "_sourcedir $(pwd)" \
	--define "_topdir $(pwd)" \
	--define "python_pkg $PYTHON_PKG" \
	--define "python_cmd $PYTHON_CMD"

# EOB
