#
#	dmftar.spec
#	Copyright 2014 SURFsara B.V.
#	Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

Summary: dmftar DMF aware tar utility
Name: dmftar
Version: %{tag_version}
Release: %{tag_release}
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: SURF Data Preservation Services <helpdesk@surf.nl>
Url: %{project_url}
Packager: %{user_name} <%{user_email}>
AutoReqProv: no
BuildRequires: %{python_pkg}, %{python_pkg}-pip, bash, tar, sed, rpm-build
%if 0%{?rhel}
Requires: %{python_pkg}, %{python_pkg}-rpm-macros, bash, tar, openssh, openssh-clients
# RHEL <= 7 does not have Suggests/Recommends
%if %{?rhel} >= 8
Suggests: %{python_pkg}-xxhash, %{python_pkg}-blake3
%endif
%else
# SUSE
Requires: %{python_pkg}, bash, tar, openssh
Recommends: %{python_pkg}-xxhash, %{python_pkg}-blake3
%endif

%description
dmftar creates and extracts data archives.
A dmftar archive may consists of multiple tar volumes.
Each volume is checksummed to validate its integrity.
dmftar is DMF aware, meaning that it takes into account the tape subsystem,
and will invoke the dmget/dmput utilities if found on the system.
The scp file transfer command is used when working with remote archives.
dmftar relies on GNU tar for its multi-volume feature.


%prep
###%setup -n dmftar-%{version}

%build
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
%{python_cmd} -m venv %{_tmppath}/rpmbuild-dmftar-venv
source %{_tmppath}/rpmbuild-dmftar-venv/bin/activate
pip3 install --upgrade pip
pip3 install -r %{_sourcedir}/requirements.txt
cd %{_sourcedir}
%if 0%{?rhel} <= 7
# git version on RHEL 7 is too old for flit
# flit will not work unless we patch it
# we can only patch it when running in a container
if [ ! -z "$CI_JOB_ID" ]
then
    sed -i "s?'--recurse-submodules', ??" %{_tmppath}/rpmbuild-dmftar-venv/lib64/python3.6/site-packages/flit/vcs/git.py
fi
%endif
flit build
deactivate

%install
cd %{_sourcedir}
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
rm -rf %{buildroot}
mkdir -p %{buildroot}
source %{_tmppath}/rpmbuild-dmftar-venv/bin/activate
%if 0%{?rhel}
pip3 install --root %{buildroot} .
%else
pip install --root %{buildroot} .
%endif
deactivate
# virtualenv pip install typically adds the "virtual root" to the paths
# therefore move the files/subdirs to the base buildroot dir
if [ -e %{buildroot}%{_tmppath}/rpmbuild-dmftar-venv ]
then
    mkdir %{buildroot}%{_prefix}
    mv %{buildroot}%{_tmppath}/rpmbuild-dmftar-venv/etc %{buildroot}/
    mv %{buildroot}%{_tmppath}/rpmbuild-dmftar-venv/* %{buildroot}%{_prefix}/
    rmdir %{buildroot}%{_tmppath}/rpmbuild-dmftar-venv %{buildroot}%{_tmppath}
    # now the generated scripts have a hashbang line that uses the virtualenv path
    # the correct hashbang line should use the system python interpreter
    sed -i "1 s?%{_tmppath}/rpmbuild-dmftar-venv/?%{_prefix}/?" %{buildroot}%{_bindir}/dmftar
    sed -i "1 s?%{_tmppath}/rpmbuild-dmftar-venv/?%{_prefix}/?" %{buildroot}%{_bindir}/dmftar-server
    sed -i "1 s?%{_tmppath}/rpmbuild-dmftar-venv/?%{_prefix}/?" %{buildroot}%{_bindir}/dmftar-volume-changer
fi
%if 0%{?rhel} <= 7
# RHEL 7 invokes /usr/bin/python to compile to .pyc files
# but /usr/bin/python is Python 2.7 and that won't work
# so we have a major hack here to switch to Python 3.6
if [ ! -z "$CI_JOB_ID" ]
then
    ln -sf python3.6 /usr/bin/python
fi
%endif

%clean
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/dmftar
%{_bindir}/dmftar-server
%{_bindir}/dmftar-volume-changer
%config(noreplace) %{_sysconfdir}/dmftar.conf
%{_mandir}/man1/dmftar.1.gz
%if 0%{?rhel}
%{python3_sitelib}/dmftar
%{python3_sitelib}/dmftar-*.dist-info
%else
# SLES12 python3_sitelib points at python3.4 which is the incorrect version
%{_prefix}/lib/python3.6/site-packages/dmftar
%{_prefix}/lib/python3.6/site-packages/dmftar-*.dist-info
%endif

%changelog
