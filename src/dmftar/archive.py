#
#   dmftar archive.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements class Archive'''

import os
import sys
import subprocess
import shutil
import errno
import re

from typing import List, Optional

from dmftar.stdio import debug, stdout, error, warning
from dmftar.batch import Batch
from dmftar.volume import Volume, RemoteVolume
from dmftar.lib import make_cmd_arr, ExecError
from dmftar.tar_env import TarEnv
from dmftar.checksum import ChecksumError
from dmftar.remote import RemoteError
from dmftar import lib, remote, filesystem, bytesize

# only used for typing
from dmftar.config import Config    # pylint: disable=unused-import


class Archive:
    '''represents an archive'''

    CMD_TAR_CREATE = ('$TAR_BINARY -c $VERBOSE -p -f $FIRST_VOLUME '
                      '-M -L $TAPELEN -F $VOLUME_CHANGER '
                      '$FORCE_LOCAL $TAR_OPTIONS')

    CMD_TAR_EXTRACT = ('$TAR_BINARY -x $VERBOSE -p -f $FIRST_VOLUME '
                       '-M -F $VOLUME_CHANGER $FORCE_LOCAL $FROM_FILE '
                       '$TAR_OPTIONS')

    def __init__(self, conf: Config, archive_dir: Optional[str] = None) -> None:
        '''initialise instance'''

        self.config = conf
        if archive_dir is None:
            archive_dir = conf.archive_dir

        if archive_dir is None:
            self.path = self.name = None
        else:
            self.path = archive_dir
            # name is last path element of archive_dir
            self.name = os.path.basename(archive_dir.strip(os.sep))

        self.remote = conf.remote

        if self.config.cache_dir is not None:
            self.cache_dir = self.config.cache_dir  # type: Optional[str]
        elif self.remote:
            assert self.name is not None
            # cache_dir is placed under current dir
            self.cache_dir = os.path.join(f'dmftar-cache.{os.getpid()}', self.name)
            self.config.cache_dir = self.cache_dir
        else:
            self.cache_dir = None

        debug(f'archive.name == {self.name}')
        debug(f'archive.path == {self.path}')
        debug(f'archive.remote == {self.remote}')
        debug(f'archive.cache_dir == {self.cache_dir}')

        # tar_subcommand is set later
        self.tar_subcommand = None  # type: Optional[str]
        self.num_volumes = 0

    def __str__(self) -> str:
        '''Returns string object: archive directory'''

        assert self.path is not None
        # the 'archive' equals the path to the archive
        return self.path

    def local_dir(self):
        '''Returns the local dir for the archive
        or for remote archives returns the local cache dir
        '''

        return str(self) if self.remote is None else self.cache_dir

    def init_from_env(self, env: TarEnv) -> None:
        '''initialize from a TarEnv object'''

        # Note: This method does not set self.config;
        # It must have been set by __init__(conf)

        assert env.tar_archive is not None
        arr = env.tar_archive.split(os.sep)
        if len(arr) < 3:
            raise EnvironmentError(errno.EINVAL, 'invalid path in TAR_ARCHIVE environment variable')

        # NOTE we get the archive.path and archive.name from config
        assert self.path is not None
        debug(f'archive.path = {self.path}')
        debug(f'archive.name = {self.name}')

        self.tar_subcommand = env.tar_subcommand

        # set number of volumes (if available)
        if 'DMFTAR_NUMVOLS' in os.environ:
            try:
                self.num_volumes = int(os.environ['DMFTAR_NUMVOLS'])
            except ValueError as err:
                raise EnvironmentError(errno.EINVAL, f"invalid value for DMFTAR_NUMVOLS: '{os.environ['DMFTAR_NUMVOLS']}'") from err
        else:
            self.num_volumes = 0
        debug(f'archive.num_volumes = {self.num_volumes}')

    def mkdir(self) -> None:
        '''make archive directory
        May raise OSError
        '''

        dstdir = str(self)
        lib.mkdir_p(dstdir)

    def volume_bynumber(self, num: int, local: bool = False) -> Volume:
        '''Returns Volume object, with given seq number
        or returns RemoteVolume object when REMOTE is set
        Raises RuntimeError for invalid volume number
        '''

        if self.tar_subcommand != '-c':
            # when not creating a new archive, disallow erroneous
            # volume numbers
            if num <= 0 or num > self.num_volumes:
                raise RuntimeError(f'invalid volume number: {num}')

        if local or self.remote is None:
            v = Volume(self)
        else:
            v = RemoteVolume(self)

        v.number = num
        return v

    def count_volumes(self, count_local: bool = False) -> None:
        '''get number of volumes
        This initialises self.num_volumes
        May raise OSError, ValueError, RemoteError
        '''

        if count_local or self.remote is None:
            self.num_volumes = self._local_count()
        else:
            self.num_volumes = self._remote_count()

        debug(f'archive.num_volumes = {self.num_volumes}')

        # put into environment for volume-changer
        os.environ['DMFTAR_NUMVOLS'] = f'{self.num_volumes}'

    def _remote_count(self) -> int:
        '''Returns number of remote volumes stored on the server
        May raise RemoteError, ValueError
        '''

        # find out how many volumes there are
        archive_dir = str(self)
        output = remote.command(self.remote, f'count {archive_dir}')

        if output is None:
            raise RemoteError('no output from remote server')

        try:
            num_volumes = int(output)
        except ValueError as err:
            raise ValueError(f"got invalid value from server: '{output.strip()}'") from err

        if num_volumes <= 0:
            raise ValueError(f'invalid number of volumes: {num_volumes}')

        return num_volumes

    def _local_count(self) -> int:
        '''Returns the number of local volumes
        This initializes self.num_volumes
        May raise OSError, ValueError
        '''

        # to get the number of volumes, we find the highest numbered subdir
        # and then find the highest numbered volume in that dir

        # local_dir is either archive.path or archive.cache_dir
        archive_dir = self.local_dir()

        if not os.path.isdir(archive_dir):
            raise OSError(errno.ENOENT, 'No such directory', archive_dir)

        if not os.path.isdir(os.path.join(archive_dir, '0000')):
            raise ValueError(f'{archive_dir}: not an archive directory')

        # volume count equals the highest volume number under directory
        return self._highest_volume(archive_dir)

    @staticmethod
    def _highest_volume(archive_dir: str) -> int:
        '''Returns highest volume number on files under archive_dir
        May raise OSError, ValueError
        '''

        # get highest numbered subdir under archive/
        archive_subdir = Archive._highest_subdir(archive_dir)
        # get the highest volume number under subdir
        return Archive._highest_volume_in_subdir(archive_subdir)

    @staticmethod
    def _highest_subdir(archive_dir: str) -> str:
        '''Returns path of highest numbered subdir under archive_dir
        Param archive_dir may be a local cache dir
        May raise OSError, ValueError
        '''

        try:
            subdirs = os.listdir(archive_dir)
        except OSError as err:
            raise OSError(err.errno, f'listdir failed: {err.strerror}', archive_dir) from err

        # find the highest numbered subdir
        highest = -1
        for dirname in subdirs:
            try:
                num = int(dirname)
            except ValueError:
                # not a number
                continue
            if num > highest:
                highest = num

        debug(f'highest == {highest}')
        if highest < 0:
            raise ValueError(f'{archive_dir}: not an archive directory')

        # return highest numbered subdir under archive/
        path = os.path.join(archive_dir, f'{highest:04}')
        return path

    @staticmethod
    def _highest_volume_in_subdir(archive_subdir: str) -> int:
        '''Returns the highest volume number that occurs in numbered subdir
        May raise OSError, ValueError
        '''

        try:
            files = os.listdir(archive_subdir)
        except OSError as err:
            raise OSError(err.errno, f'listdir failed: {err.strerror}', archive_subdir) from err

        # filter out index and checksum files
        # so that only tar volume files are left
        files = [x for x in files if not (x.endswith('.idx') or x.endswith('.chksum'))]

        # recognize a volume file
        regex_volume = re.compile(r'\.tar(-(\d+))?$')

        highest = 0
        for filename in files:
            m = regex_volume.search(filename)
            if m is None:
                # unrecognized file
                continue

            _, s_num = m.groups()
            if s_num is None:
                # first volume has no number
                num = 1
            else:
                try:
                    num = int(s_num)
                except ValueError:
                    # shouldn't happen because regex has pattern \d+
                    continue

            if num > highest:
                highest = num

        debug(f'highest == {highest}')
        if highest < 1:
            raise ValueError(f'{archive_subdir}: no volumes found')

        return highest

    def create(self, patterns: List[str]) -> None:
        '''create the archive
        May raise ExecError, OSError
        '''

        self.tar_subcommand = '-c'
        self.check_archive_already_exists()
        self._do_create(patterns)
        self.cleanup_environment()

    def check_archive_already_exists(self) -> None:
        '''check whether archive dir already exists
        We want it to not exist, because we don't want to overwrite
        existing archives on create

        May raise RuntimeError, OSError
        '''

        if self.remote is None:
            if os.path.exists(str(self)):
                raise RuntimeError(f'{self} already exists')

        else:
            # check whether remote archive already exists
            output = remote.command(self.remote, 'exists ' + str(self))
            assert output is not None   # this helps mypy
            output = output.strip()
            debug(f'got server response: [{output}]')
            if output == 'OK':
                # do not overwrite remote archive
                raise RuntimeError(f'{self.remote}:{self} already exists')

            if output != 'NACK':
                debug(f'server output = {output}')
                raise ValueError('unexpected message from server')

        # it does not yet exist
        # everything OK

    def _do_create(self, patterns: List[str]) -> None:
        '''create an archive from files or directory
        May raise ExecError, OSError
        '''

        # first, define a local cleanup function
        def _cleanup_broken_archive(archive_path: str) -> None:
            '''delete (broken) archive
            This will delete the entire archive directory

            May print error message, but these errors are considered non-fatal
            '''

            if not os.path.isdir(archive_path):
                return

            if archive_path in ('/', '.'):
                error(f"cowardly refusing to delete '{self.path}'")
                return

            # delete entire tree
            debug(f'deleting broken archive: {archive_path}')
            try:
                shutil.rmtree(archive_path)
            except OSError as err:
                error(err.strerror)
                return
            #return (local func ends here)

        debug(f'creating archive: {self}')

        if self.remote is None:
            debug('creating Volume')
            vol = Volume(self)
        else:
            debug('creating RemoteVolume')
            # remote archive gets created in a local cache dir
            vol = RemoteVolume(self)

        debug(f'volume == {vol}')

        tarvolume_filename = str(vol)
        archive_dir = self.local_dir()
        try:
            vol.mkdir()
        except (OSError, RuntimeError) as err:
            _cleanup_broken_archive(archive_dir)
            # re-raise the exception
            raise err

        # bring files online
        # make a batch of files and dmget them
        batch = Batch(archive_dir, self.config)
        try:
            stdout('analyzing fileset')
            for pattern in patterns:
                batch.make(pattern)
        except (ExecError, OSError) as err:
            _cleanup_broken_archive(archive_dir)
            raise err

        # dmget the first batch
        batch.dmget(1)
        # run dmget for second batch, too (DMF gets a head start)
        batch.dmget(2)

        s_patterns = ' '.join(patterns)
        if self.remote is None:
            stdout(f'tarring {s_patterns} to {self}')
        else:
            stdout(f'tarring {s_patterns} to {self.remote}:{self}')
        # exec tar
        try:
            self._run_tar_create(patterns, tarvolume_filename)
            self._create_final_volume()
        except ExecError as err:
            warning(f'leaving behind a half-finished archive in {archive_dir}')
            raise err

    def _run_tar_create(self, patterns: List[str], tarvolume_filename: str) -> None:
        '''execute tar command to create volume
        May raise ExecError
        '''

        verbose = ''
        if self.config.verbose:
            verbose = '-v'

        force_local = ''
        if self.config.force_local:
            force_local = '--force-local'

        # tar expects tape-length in KiB
        tape_length = self.config.volume_size // 1024
        if tape_length <= 0:
            tape_length = 1

        cmd_arr = make_cmd_arr(Archive.CMD_TAR_CREATE,
                               {'$TAR_BINARY': self.config.tar_binary,
                                '$VERBOSE': verbose,
                                '$FIRST_VOLUME': tarvolume_filename,
                                '$TAPELEN': f'{tape_length}',
                                '$VOLUME_CHANGER': self.config.volume_changer,
                                '$FORCE_LOCAL': force_local,
                                '$TAR_OPTIONS': self.config.tar_options})
        # append files/directories to tar command
        cmd_arr.extend(patterns)
        debug('cmd_arr == {!r}'.format(cmd_arr))
        debug('running ' + ' '.join(cmd_arr))
        sys.stdout.flush()
        try:
            ret = subprocess.call(cmd_arr, shell=False)
        except OSError as err:
            raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

        if ret >= 2:
            prog = os.path.basename(cmd_arr[0])
            raise ExecError(f'{prog} exited with return code {ret}')

        if ret == 1:
            prog = os.path.basename(cmd_arr[0])
            warning(f'{prog} exited with a warning')

    def _create_final_volume(self) -> None:
        '''run volume-changer for the final volume
        because tar does not run it by itself
        May raise ExecError
        '''

        # how many volumes do we have?
        self.count_volumes(count_local=True)

        vol = self.volume_bynumber(self.num_volumes)

        # put params in environment variables
        env = TarEnv()
        env.tar_subcommand = '-c'
        env.tar_archive = str(vol)
        env.tar_volume = vol.number + 1
        env.tar_fd = -1
        env.putenv()

        debug('running final volume-changer')
        sys.stdout.flush()
        try:
            ret = subprocess.call([self.config.volume_changer, ], shell=False)
        except OSError as err:
            raise ExecError(f'failed to run {self.config.volume_changer}: {err.strerror}') from err

        # this cleanup is run by the final volume-changer
        #vol.cleanup_cache()
        self.cleanup_cachedir()

        # volume-changer potentially leaves behind an empty volume subdir
        # if so, clean it up
        subdir = os.path.dirname(str(vol))
        try:
            os.rmdir(subdir)
        except OSError:
            pass
        if os.path.exists(subdir):
            vol.lock_directory()

        if ret != 0:
            prog = os.path.basename(self.config.volume_changer)
            raise ExecError(f'{prog} exited with return code {ret}')

    def do_list(self) -> None:
        '''list archive contents
        May raise OSError, RemoteError
        '''

        self.tar_subcommand = '-t'
        # no cleanup needed; only displaying index files
        self.list_index()

    def list_index(self) -> None:
        '''display archive contents
        This displays all index files
        May raise OSError, RemoteError
        '''

        # first find out how many volumes or index files there are
        self.count_volumes()

        # cat index files

        if self.remote is None:
            # dmget first volume.idx
            vol = self.volume_bynumber(1)
            vol.dmget_idx()

        volnum = 1
        while volnum <= self.num_volumes:
            # cat the index
            stdout(f'volume #{volnum}')
            vol = self.volume_bynumber(volnum)

            if self.remote is None:
                if volnum + 1 <= self.num_volumes:
                    # dmget the next volume.idx
                    next_vol = self.volume_bynumber(volnum + 1)
                    next_vol.dmget_idx()

                lib.cat_file(f'{vol}.idx')

            else:
                # list remote index
                assert isinstance(vol, RemoteVolume)    # this helps mypy
                output = remote.command(self.remote, f'index {vol.remote_filename()}')
                if output is not None:
                    sys.stdout.write(output)
                    sys.stdout.flush()

            volnum += 1

    def extract(self, patterns: Optional[List[str]] = None) -> None:
        '''extract the archive
        patterns can be a list of globbing patterns to extract
        May raise ExecError
        '''

        self.tar_subcommand = '-x'
        self._do_extract(patterns)
        self.cleanup_environment()

    def _do_extract(self, patterns: Optional[List[str]] = None) -> None:
        '''extract an archive
        patterns can be a list of globbing patterns to extract
        May raise ExecError
        '''

        debug(f'extracting archive: {self}')

        # first find out how many volumes there are
        self.count_volumes()

        # start by bringing volume online
        vol = self.volume_bynumber(1)

        # stage the first volume
        # this will automatically dmget volume #2
        vol.stage()

        stdout(f'extracting {self}')

        # make tar -x command and execute it
        verbose = ''
        if self.config.verbose:
            verbose = '-v'

        if self.config.force_local:
            force_local = '--force-local'
        else:
            force_local = ''

        if self.config.from_file:
            from_file = '--files-from=' + self.config.from_file
        else:
            from_file = ''

        tarfile = str(vol)

        cmd_arr = make_cmd_arr(Archive.CMD_TAR_EXTRACT,
                               {'$TAR_BINARY': self.config.tar_binary,
                                '$VERBOSE': verbose,
                                '$FIRST_VOLUME': tarfile,
                                '$FORCE_LOCAL': force_local,
                                '$FROM_FILE': from_file,
                                '$TAR_OPTIONS': self.config.tar_options,
                                '$VOLUME_CHANGER': self.config.volume_changer})

        if from_file and self.config.opt_null:
            # also pass these options when --files-from0= is used
            # they must come before --files-from=
            cmd_arr.insert(1, '--no-unquote')
            cmd_arr.insert(2, '--null')

        if patterns:
            cmd_arr.extend(patterns)

        debug('cmd_arr == {!r}'.format(cmd_arr))
        debug('running ' + ' '.join(cmd_arr))
        sys.stdout.flush()
        try:
            ret = subprocess.call(cmd_arr, shell=False)
        except OSError as err:
            raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

        if ret >= 2:
            prog = os.path.basename(cmd_arr[0])
            raise ExecError(f'{prog} exited with return code {ret}')

        if ret == 1:
            prog = os.path.basename(cmd_arr[0])
            warning(f'{prog} exited with a warning')

        # cleanup cache of final volume
        vol = self.volume_bynumber(self.num_volumes)
        vol.cleanup_cache()
        self.cleanup_cachedir()

    def download(self) -> None:
        '''download the archive
        May raise ValueError, ChecksumError, OSError
        '''

        self.tar_subcommand = '--download'
        self._do_download()
        self.cleanup_environment()

    def _do_download(self) -> None:
        '''download archive to local disk
        May raise ValueError, ChecksumError, OSError
        '''

        if self.remote is None:
            raise ValueError('no remote address given')

        # first find out how many volumes there are
        self.count_volumes()

        vol = self.volume_bynumber(1)
        assert isinstance(vol, RemoteVolume)    # this helps mypy
        stdout(f'downloading {self.num_volumes} volumes to {self.cache_dir}')

        volnum = 1
        while volnum <= self.num_volumes:
            vol = self.volume_bynumber(volnum)
            vol.stage()
            volnum += 1

        stdout(f'{self.name} download finished')

        # this will move the cache to destination dir
        self.config.keep_cache = True
        self.cleanup_cachedir()

    def verify(self) -> bool:
        '''verify all volume checksums
        Returns True if checksums check out
        May raise OSError, ValueError, RuntimeError, RemoteError
        '''

        self.tar_subcommand = '--verify'
        try:
            ret = self._do_verify()
            return ret
        finally:
            self.cleanup_environment()

    def _do_verify(self) -> bool:
        '''verify checksum of all volumes in archive
        Returns True if checksums are OK
        May raise OSError, ValueError, RuntimeError, RemoteError
        '''

        # first find out how many volumes there are
        self.count_volumes()

        stdout(f'verifying checksum for {self.num_volumes} volumes')
        vol = None
        try:
            volnum = 1
            while volnum <= self.num_volumes:
                vol = self.volume_bynumber(volnum)

                # the act of staging verifies the volume checksum
                try:
                    vol.stage(missing_checksum_is_error=True)
                except ChecksumError as err:
                    error(str(err))
                    return False
                finally:
                    vol.cleanup_cache()

                volnum += 1
        finally:
            if vol is not None:
                self.cleanup_cachedir()

        stdout('OK')
        return True

    def regenerate(self) -> None:
        '''re-create index and/or checksum files
        May raise OSError, ValueError, RuntimeError, ChecksumError, RemoteError
        '''

        self.tar_subcommand = '--regen'
        self._do_regenerate()
        self.cleanup_environment()

    def _do_regenerate(self) -> None:
        '''recreate index + checksum files
        May raise OSError, ValueError, RuntimeError, ChecksumError, RemoteError
        '''

        if self.remote is not None:
            raise RuntimeError('regenerate is not yet implemented for remote archives')

        # first find out how many volumes there are
        self.count_volumes()

        options = 0
        if self.config.regen_index:
            options += 1
        if self.config.regen_checksum:
            options += 2
        assert options != 0
        text_options = ('', 'index', 'checksum', 'index and checksum')
        stdout(f'regenerating {text_options[options]} files for {self.num_volumes} volumes')
        vol = None
        try:
            volnum = 1
            while volnum <= self.num_volumes:
                vol = self.volume_bynumber(volnum)
                vol.unlock_directory()
                try:
                    vol.stage()

                    if self.config.regen_index:
                        vol.recreate_index()

                    if self.config.regen_checksum:
                        vol.recreate_chksum()

                except (OSError, ValueError, RuntimeError, ChecksumError, RemoteError) as err:
                    vol.lock_directory()
                    vol.cleanup_cache()
                    raise err

                vol.lock_directory()
                vol.cleanup_cache()
                volnum += 1
        finally:
            if vol is not None:
                self.cleanup_cachedir()

    def delete(self) -> None:
        '''delete archive
        May raise OSError, ExecError, RemoteError
        '''

        self.tar_subcommand = '--delete-archive'

        if self.remote is not None:
            debug('delete remote archive')
            self.delete_remote()
        else:
            debug('delete local archive')
            self.delete_local()

    def delete_local(self) -> None:
        '''delete local archive from disk
        May raise OSError
        '''

        def _remove_dir(dirname: str) -> None:
            '''local func: delete directory
            May raise OSError
            '''

            stdout(f'deleting {dirname}')
            try:
                debug(f'rmdir {dirname}')
                os.rmdir(dirname)
            except OSError as err:
                if err.errno == errno.ENOENT:
                    # silently ignore (unless debug mode)
                    debug(f'rmdir {dirname}: error: {err.strerror}')
                else:
                    raise OSError(err.errno, f'failed to remove directory {dirname}: {err.strerror}') from err
            #return; local func ends here

        # first find out how many volumes there are
        self.count_volumes()

        unlock_dir = True

        # delete 'back to front'
        volnum = self.num_volumes
        while volnum >= 1:
            vol = self.volume_bynumber(volnum)

            if unlock_dir:
                # set directory writeable
                vol.unlock_directory()
                unlock_dir = False

            filename = str(vol)

            # delete checksum file
            try:
                debug(f'unlink {filename}.chksum')
                os.unlink(f'{filename}.chksum')
            except OSError as err:
                # silently ignore any error (unless debug mode)
                debug(f'unlink {filename}.chksum: error: {err.strerror}')

            # delete index
            try:
                debug(f'unlink {filename}.idx')
                os.unlink(f'{filename}.idx')
            except OSError as err:
                # silently ignore any error (unless debug mode)
                debug(f'unlink {filename}.idx: error: {err.strerror}')

            # delete volume file
            try:
                debug(f'unlink {filename}')
                os.unlink(filename)
            except OSError as err:
                if err.errno == errno.ENOENT:
                    # silently ignore (unless debug mode)
                    debug(f'unlink {filename}: error: {err.strerror}')
                else:
                    raise OSError(err.errno, f'failed to delete {filename}: {err.strerror}') from err

            if volnum % 1000 == 0:
                # delete the volume directory
                dirname = os.path.dirname(filename)
                _remove_dir(dirname)
                unlock_dir = True

            volnum -= 1

        # delete the toplevel archive directory
        dirname = str(self)
        # because the first volume is 1 (and not zero) the last subdir
        # is not removed by the loop above. So remove it now
        _remove_dir(os.path.join(dirname, '0000'))
        _remove_dir(dirname)

    def delete_remote(self) -> None:
        '''let server delete remote archive
        May raise ExecError, RemoteError
        '''

        assert self.remote is not None

        archive_dir = str(self)
        output = remote.command(self.remote, f'delete-archive {archive_dir}')

        # workaround; stdout() will add an extra newline
        if output and output[-1] == '\n':
            output = output[:-1]
        if output:
            stdout(output)

    @staticmethod
    def cleanup_environment() -> None:
        '''unset TAR_xxx environment variables'''

        for var in ('TAR_SUBCOMMAND', 'TAR_ARCHIVE', 'TAR_VOLUME', 'TAR_FD'):
            try:
                del os.environ[var]
            except KeyError:
                pass

    def cleanup_cachedir(self) -> None:
        '''remove top-level local cache dir'''

        if not self.remote:
            # this is a no-op for local archives
            return

        assert self.cache_dir is not None   # this helps mypy

        if self.config.keep_cache:
            debug(f'keep-cache, not removing {self.cache_dir}')
            assert self.name    # this helps mypy
            debug(f'rename {self.cache_dir} -> {self.name}')
            try:
                os.rename(self.cache_dir, self.name)
            except OSError as err:
                error(f'failed to move {self.cache_dir}/ to {self.name}/ : {err.strerror}')
                return

            # the toplevel cache_dir is still there
            # we will remove that next

        # remove cache_dir and everything under it
        # this is a little scary so we do an extra safety check
        cache_dir = os.path.dirname(self.cache_dir)
        if cache_dir.startswith('dmftar-cache.') and os.path.isdir(cache_dir):
            try:
                debug(f'rmdir {cache_dir}')
                shutil.rmtree(cache_dir)
            except OSError as err:
                warning(f'failed to cleanup {cache_dir}/ : {err.strerror}')

    def make_volume_batch(self, start_number: int) -> List[Volume]:
        '''make list of volumes to get as a single batch of volumes'''

        if self.remote is not None:
            # will not do remote dmget;
            # dmftar-server will do it "locally" for the remote server
            return []

        volume_list = []   # type: List[Volume]

        # only get volumes for which no dmget has been issued yet
        # volbatch_start is a var passed to volume-changer through
        # a temp config file
        if start_number < self.config.volbatch_start:
            debug(f'dmget already issued for volume #{start_number}')
            return []

        batch_size = 0

        idx = start_number
        while idx <= self.num_volumes:
            vol = self.volume_bynumber(idx)
            size = vol.getsize()
            if batch_size > 0 and batch_size + size > self.config.volbatch_size:
                break

            batch_size += size
            volume_list.append(vol)
            idx += 1

        debug(f'batch_size == {bytesize.sprint(batch_size)}')

        if idx > self.config.volbatch_start:
            # save the volume number where we left off
            # the next batch starts at the next volume index
            debug(f'next volbatch_start := {idx}')
            self.config.volbatch_start = idx
            self.config.putenv()

        return volume_list

    def dmget_volume_batch(self, volume_list: List[Volume], index_only: bool = False) -> None:
        '''issue dmget to bring list of volumes online
        May raise ExecError, OSError
        '''

        # Note: we only do dmget here
        # checksum verification is handled elsewhere

        if not volume_list:
            debug('volume list is empty')
            return

        if self.config.pipe_cmd_dmget_batch is None:
            debug('config.pipe_cmd_dmget_batch is None')
            return

        filename = str(volume_list[0])
        if not filesystem.dmf_capable(filename):
            debug('not a DMF capable filesystem')
            return

        # make list of null-terminated filenames
        # bring online both volume + index files
        # checksum files are too small to be migrated
        arr = []    # type: List[str]
        for vol in volume_list:
            arr.append(f'{vol}.idx')
            if not index_only:
                arr.append(str(vol))
        # null-terminated list
        input_str = '\0'.join(arr) + '\0'

        # execute dmget
        self._do_pipe_dmget(input_str)

    def _do_pipe_dmget(self, input_str: str) -> None:
        '''Execute dmget, piping input string into dmget
        May raise OSError, ExecError
        '''

        # this was already checked before
        # this helps mypy
        assert self.config.pipe_cmd_dmget_batch is not None

        cmd_pipe_dmget = self.config.pipe_cmd_dmget_batch.split()
        debug('cmd_pipe_dmget == {!r}'.format(cmd_pipe_dmget))
        debug('running ' + ' '.join(cmd_pipe_dmget))
        sys.stdout.flush()
        try:
            # Note: dmget does not have any good return value, so use check=False
            # Any output from dmget will appear on stdout/stderr
            # because we don't redirect those
            subprocess.run(cmd_pipe_dmget, shell=False, input=input_str,
                           universal_newlines=True, check=False)

        except OSError as err:
            raise lib.ExecError(f'failed to run {cmd_pipe_dmget[0]}: {err.strerror}')

# EOB
