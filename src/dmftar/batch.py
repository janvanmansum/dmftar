#
#   dmftar batch.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements class Batch, which is a list of files that is to be
staged online with dmget before they can be tarred
'''

import os
import sys
import subprocess
import errno

from typing import List

from dmftar.stdio import stdout, error, debug
from dmftar.filesystem import dmf_capable
from dmftar import lib

# only used for typing:
from dmftar.config import Config    # pylint: disable=I0011,W0611


class Batch:
    '''represents a list of files for a volume,
    that are to be read from tape with dmget
    The filelist is kept in a temp file
    '''

    TAR_HEADER_SIZE = 512

    def __init__(self, path: str, conf: Config) -> None:
        '''initialise instance'''

        self.path = path
        self.config = conf
        self.volnum = 1
        self.size = Batch.TAR_HEADER_SIZE
        self.filelist: List[str] = []

    def __str__(self) -> str:
        '''Returns string object; the filename'''

        return os.path.join(self.path, 'tmp', f'{self.volnum // 1000:04}',
                            f'batch-{self.volnum}.tmp')

    def make(self, pattern: str) -> None:
        '''make set of 'batch' files
        Pattern is a directory or file globbing pattern
        Batch files contain file lists, needed for dmget to get to work
        May raise ExecError, OSError
        '''

        # exec find -print0
        cmd_arr = lib.make_cmd_arr(self.config.cmd_find_batch, {'$SRCDIR': pattern})
        debug('cmd_arr == {!r}'.format(cmd_arr))
        debug('running ' + ' '.join(cmd_arr))
        sys.stdout.flush()

        num_errors = 0
        with subprocess.Popen(cmd_arr, bufsize=-1, shell=False, stdout=subprocess.PIPE) as proc:
            f = proc.stdout
            assert f is not None                    # this helps mypy

            # read nul-terminated filenames
            # add them to this batch

            for filename in lib.read_nul_strings(f):
                if not filename:
                    # (this should not happen, but hey)
                    continue

                try:
                    self.check_access(filename)
                except OSError as err:
                    # print error message and continue for now
                    if hasattr(err, 'filename') and err.filename is not None:
                        error(f'{err.filename}: {err.strerror}')
                    else:
                        error(err.strerror)
                    num_errors += 1

                self.add(filename)

            # sync the batch to disk (write to temp file)
            self.sync()

            proc.wait()

            if num_errors:
                # there were (a number of) errors
                raise lib.SilentError

            if proc.returncode != 0:
                prog = os.path.basename(cmd_arr[0])
                raise lib.ExecError(f'{prog} exited with return code {proc.returncode}')

    @staticmethod
    def check_access(filename: str) -> None:
        '''check access to file or directory
        May raise OSError
        '''

        # check readability of the file
        # No access to a file or directory is an error

        if not os.path.exists(filename):
            raise OSError(errno.ENOENT, 'No such file or directory', filename)

        if os.path.isdir(filename):
            if not os.access(filename, os.R_OK | os.X_OK):
                raise OSError(errno.EACCES, 'Permission denied', filename)

        elif os.path.islink(filename):
            # ignore mode on symbolic links
            # In Linux, the mode of the symlink is always 0777
            # Also note that it may be a dead symlink
            pass
        else:
            if not os.access(filename, os.R_OK):
                raise OSError(errno.EACCES, 'Permission denied', filename)

    def add(self, filename: str) -> None:
        '''add filename to filelist, splitting the volume as appropriate
        May raise ValueError, OSError, RuntimeError
        '''

        if self.config.volume_size < 0:
            raise ValueError(f'illegal volume size: {self.config.volume_size}')

        # Note: it's sufficient to put only offline (OFL) files on this list
        # but getting the migration state in Python is not efficient
        # Luckily, dmget will not complain if state is REG or DUL
        if os.path.isfile(filename):
            self.filelist.append(filename)

        size = tarred_size(filename)
#        debug(f'{size:12}  {filename}')

        # split volume
        # note that a file may stretch over multiple volumes if
        # it is a very large file
        self.size += size
        while self.size >= self.config.volume_size:
            self.sync()

            # remaining size
            size = self.size - self.config.volume_size

            # take tar blocks into account
            if size % 1024 > 0:
                n = size // 1024
                size = (n + 1) * 1024

            if len(filename) <= 100:
                size += 512             # plus header
            else:
                size += 3 * 512         # extended header

            # reset this batch
            self.size = Batch.TAR_HEADER_SIZE + size
            self.volnum += 1

            # current file is continued in the next volume
            # but no need to put it on filelist again
            # because DMF will already dmget it
            self.filelist = []

    def sync(self) -> None:
        '''write filelist to (temp) file
        May raise OSError, RuntimeError
        '''

        debug(f'creating batch #{self.volnum}')

        filename = str(self)
        # make directory
        dirname = os.path.dirname(filename)
        lib.mkdir_p(dirname)

        # write the file
        try:
            with open(filename, 'w+b') as f:
                for line in self.filelist:
                    if not dmf_capable(line):
                        # no need to run dmget for this file
                        continue

                    # write nul-terminated filenames to file
                    line += '\x00'
                    data = line.encode()
                    f.write(data)

        except OSError as err:
            raise OSError(err.errno, f'write failed: {err.strerror}', filename) from err

        # no need to keep this in memory
        self.filelist = []

    def dmget(self, volnum: int) -> None:
        '''invoke dmget for volume number to get a batch of files online
        May raise ExecError, OSError
        '''

        #debug(f'dmgetting batch #{volnum}')
        self.volnum = volnum

        if self._prepare_dmget():
            self._do_dmget()

        # afterwards, delete the temp file
        self.cleanup()

    def _prepare_dmget(self) -> bool:
        '''do some checks before running batch dmget
        Returns False if we can not run dmget
        '''

        if self.config.cmd_dmget is None:
            # no dmget command on this system
            return False

        if self.config.pipe_cmd_dmget_batch is None:
            # can not do piped dmget
            return False

        arr = self.config.cmd_dmget.split()
        if not os.access(arr[0], os.X_OK):
            # dmget: command not found
            # this is not a hard error; ignore it
            return False

        filename = str(self)
        if not os.path.isfile(filename):
            # no such file
            # this may happen because batch.dmget() is far ahead of tar
            # it's OK, this is no hard error
            return False

        # batches may be empty if the files are not
        # on a DMF capable filesystem
        try:
            filesize = os.path.getsize(filename)
            if filesize <= 0:
                debug(f'batch #{self.volnum} is empty; no files to dmget')
                return False
        except OSError as err:
            debug(f'getsize({filename}): {err.strerror}')

        # is this file on a DMF filesystem?
        return dmf_capable(filename)

    def _do_dmget(self) -> None:
        '''dmget a batch of files
        May raise OSError
        '''

        stdout(f'dmgetting batch #{self.volnum}')
        prog = 'dmget'
        batch_filename = str(self)
        try:
            with open(batch_filename, 'rb') as f:
                # go run pipe dmget -0
                # the input to dmget are nul-terminated strings from the batch data file

                # this was already checked in _prepare_dmget()
                # this helps mypy
                assert self.config.pipe_cmd_dmget_batch is not None

                cmd_pipe_dmget = self.config.pipe_cmd_dmget_batch.split()
                prog = os.path.basename(cmd_pipe_dmget[0])
                debug('running ' + ' '.join(cmd_pipe_dmget))
                sys.stdout.flush()

                try:
                    subprocess.run(cmd_pipe_dmget, shell=False, stdin=f, check=True)

                except subprocess.CalledProcessError:
                    # ignore the returncode of dmget
                    # dmget does not have a well-documented return value
                    pass

        except OSError as err:
            raise OSError(err.errno, f'{prog} {batch_filename} failed: {err.strerror}') from err

    def cleanup(self) -> None:
        '''delete batch file'''

        filename = str(self)
        debug(f'cleanup: deleting {filename}')
        try:
            os.unlink(filename)
        except OSError:
            # silently ignore unlink errors
            pass
        else:
            # try cleanup empty dir
            number_dir = os.path.dirname(filename)
            try:
                os.rmdir(number_dir)
            except OSError:
                # silently ignore error
                # probably directory not empty
                pass
            else:
                # try cleanup empty tmp dir
                tmp_dir = os.path.join(self.path, 'tmp')
                try:
                    os.rmdir(tmp_dir)
                except OSError:
                    # silently ignore error
                    # probably directory not empty
                    pass



def tarred_size(filename: str) -> int:
    '''Returns the size of file + tar header
    It is a rough estimate
    May raise OSError
    '''

    size = 0

    if os.path.isfile(filename) and not os.path.islink(filename):
        # this might raise OSError
        size = os.path.getsize(filename)

    # tar uses blocks
    if size % 1024 > 0:
        n = size // 1024
        size = (n + 1) * 1024

    if len(filename) <= 100:
        size += 512             # plus header
    else:
        size += 3 * 512         # extended header

    return size

# EOB
