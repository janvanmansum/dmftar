#
#   dmftar bytesize.py  WJ118
#   Copyright 2018 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''parse and pretty-print kB, kiB, MB, MiB, GB, GiB, etc.'''

import re

# values greater than SMALL_NUMBER will be formatted as
# a fraction of the next multiplier; e.g. "0.9 GB"
SMALL_NUMBER = 850


def parse_multiplier(svalue: str) -> int:
    '''Parse metric like "B", "kiB", "MiB", "GB", etc.
    Returns the multiplier value
    Raises ValueError for invalid metric passed
    '''

    if svalue is None or not svalue:
        # assume bytes
        return 1

    orig = svalue
    svalue = svalue.upper()

    if svalue[-1] == 'B':
        svalue = svalue[:-1]
        if not svalue:
            # it is in bytes
            return 1

    # we use uppercase, so check for capital 'I'
    if svalue[-1] == 'I':
        factor = 1024
        svalue = svalue[:-1]
    else:
        factor = 1000

    if len(svalue) != 1:
        raise ValueError(f'invalid metric: {orig}')

    # we use uppercase, so note the capital 'K' in here
    si_prefix = ' KMGTPEZY'
    n = si_prefix.index(svalue)
    return factor ** n


def parse(svalue: str) -> int:
    '''Parse formatted string with kB/MB/GB into integer
    and return that integer value
    '''

    if not svalue:
        raise ValueError(f"invalid value: '{svalue}'")

    parse_value = svalue.lower()
    if len(parse_value) >= 2 and (parse_value[-2:] == 'ps' or parse_value[-2:] == '/s'):
        parse_value = parse_value[:-2]

    # regex matches int or float + SI prefix
    # interesting answers are in group 0 and 2
    m = re.match(r'(\d+(\.\d+)?)\s*([kmgtpezy]?(ib|b)?)$', parse_value)
    if m is None:
        raise ValueError(f"invalid value: '{svalue}'")
    s_num, _, s_prefix, _ = m.groups()

    if s_prefix == 'ib':
        raise ValueError(f"invalid value: '{svalue}'")
    if s_prefix == 'b':
        s_prefix = ''       # bytes

    if not s_prefix:
        # it's bytes
        if '.' in s_num or ',' in s_num:
            # it should have been an integer
            raise ValueError(f"invalid value: '{svalue}'")
        ivalue = int(s_num)
        return ivalue

    fvalue = float(s_num)
    multiplier = parse_multiplier(s_prefix)
    ivalue = int(fvalue * multiplier)
    return ivalue


def sprint(value: int, factor: int = 1000) -> str:
    '''Return value formatted as string
    Default factor is 1000; pass factor=1024 for kiB, MiB, GiB
    '''

    if factor == 1000:
        suffix = 'B'
    elif factor == 1024:
        if value < SMALL_NUMBER:
            suffix = 'B'
        else:
            suffix = 'iB'
    else:
        raise RuntimeError(f'invalid factor: {factor}')
    return sprint_prefix(value, factor) + suffix


def sprint_prefix(n: int, factor: int = 1000) -> str:
    '''Returns string that shows number n
    with SI prefix appended
    '''

    # hack for small numbers; the loop below will always
    # print a floating point number
    if n < SMALL_NUMBER:
        return f'{n} '

    for x, a_prefix in reversed(list(enumerate(' kMGTPEZY'))):
        big_x = factor ** x
        if n >= big_x:
            fvalue = n / float(big_x)
            return f'{fvalue:.1f} {a_prefix}'

        fraction = n / float(big_x)
        rest = fraction - int(fraction)
        if rest >= 0.85:
            return f'{fraction:.1f} {a_prefix}'
    # not reached, but hey
    return f'{n} '

# EOB
