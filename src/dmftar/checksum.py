#
#   dmftar checksum.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''file checksums'''

import os
import hashlib
import subprocess
import abc

if 'blake3' not in hashlib.algorithms_available:
    try:
        import blake3                                                   # type: ignore # mypy doesn't like this
        HAVE_BLAKE3_LIB = True
    except ImportError:
        HAVE_BLAKE3_LIB = False
else:
    HAVE_BLAKE3_LIB = False

try:
    import xxhash                                                       # type: ignore # mypy doesn't like this
    HAVE_XXHASH_LIB = True
except ImportError:
    HAVE_XXHASH_LIB = False

from typing import List, Dict, Optional, Any, Callable

from dmftar.stdio import warning, debug
from dmftar import lib


class SummerInterface(abc.ABC):
    '''defines interface for "checksumming calculator engine"s'''

    def calc(self, filename: str) -> str:                               # pylint: disable=no-self-use
        '''Calculates checksum for a file, using the algorithm
        specified for the engine
        Returns the hexdigest as string
        '''

        # Note that the algorithm is not part of this interface
        # but in general the Summer will have to know an algorithm

        raise RuntimeError('abstract method called')


# dict of supported algorithms
# maps name => checksumming calculator instance: Summer|ExternalSummer
# this dict must be defined as empty;
# it is programmatically filled by init_map_algorithms()
ALGORITHMS: Dict[str, SummerInterface] = {}


def algorithms() -> List[str]:
    '''Returns list of available algorithms'''

    if not ALGORITHMS:
        init_map_algorithms()

    debug(f'{list(ALGORITHMS.keys())}')
    return list(ALGORITHMS.keys())


def init_map_algorithms() -> None:
    '''initialize map ALGORITHMS'''

    if ALGORITHMS:
        debug('ALGORITHMS was already initialized')
        return

    init_hashlib_algorithms()
    init_blake3_algorithm()
    init_xxhash_algorithms()


def init_hashlib_algorithms() -> None:
    '''add hashlib algorithms to available ALGORITHMS'''

    # hashlib offers .algorithms_guaranteed
    # however it includes variable length digests
    # which is an additional issue because the digest requires a length
    # so for simplicity we don't support variable length checksums;
    # they are filtered out
    for algo in hashlib.algorithms_guaranteed:
        if ' ' in algo:
            # our .chksum save-file format does not support spaces in algorithm names
            continue
        try:
            _ = hashlib.new(algo).hexdigest()
            ALGORITHMS[algo] = Summer(algo, hashlib.new)
        except (TypeError, ValueError):
            pass


def init_blake3_algorithm() -> None:
    '''add blake3 algorithm to available ALGORITHMS
    (if it is present on the system)
    '''

    # Note: these debugs typically run _before_ DEBUG has been set True
    # and are therefore invisible unless you patch `DEBUG=True` in stdio.py

    if 'blake3' in hashlib.algorithms_available:
        debug('blake3 is in hashlib')
        ALGORITHMS['blake3'] = Summer('blake3', hashlib.new)

    elif HAVE_BLAKE3_LIB:
        debug('blake3 is supported by module')
        ALGORITHMS['blake3'] = Summer('blake3', blake3_new)
    else:
        init_b3sum_external_algorithm()


def init_b3sum_external_algorithm() -> None:
    '''add external blake3 algorithm to available ALGORITHMS'''

    assert not HAVE_BLAKE3_LIB
    assert 'blake3' not in hashlib.algorithms_available

    # blake3 support via external commands
    cmd = 'b3sum'
    try:
        # find 'b3sum' command on PATH
        _ = lib.search_path(cmd)
        ALGORITHMS['blake3'] = ExternalSummer('blake3', cmd)
        debug(f'blake3 support via external command: {cmd}')
    except ValueError:
        debug('blake3 is not available')


def blake3_new(_algo: str) -> Any:
    '''Returns a new hashlib-API capable instance for blake3 algorithm

    This method works only if module blake3 is available
    '''

    # module blake3 has no good `.new('algo')` method
    # so we implement our own

    assert HAVE_BLAKE3_LIB

    return blake3.blake3()                                              # pylint: disable=not-callable


def init_xxhash_algorithms() -> None:
    '''add xxhash algorithms to available ALGORITHMS
    (if they are present on the system)
    '''

    # Note: these debugs typically run _before_ DEBUG has been set True
    # and are therefore invisible unless you patch `DEBUG=True` in stdio.py

    if HAVE_XXHASH_LIB:
        # xxhash support via module
        for algo in ('xxh32', 'xxh64', 'xxh3_64', 'xxh128'):
            if hasattr(xxhash, algo):
                debug(f'{algo} is in module xxhash')
                ALGORITHMS[algo] = Summer(algo, xxhash_new)
    else:
        init_xxhash_external_algorithms()


def xxhash_new(algo: str) -> Any:
    '''Returns a new hashlib-API capable instance for xxhash algorithms
    May raise ValueError

    This method works only if module xxhash is available
    '''

    # module xxhash has no `.new('algo')` method
    # so we implement our own

    assert HAVE_XXHASH_LIB

    try:
        classname = getattr(xxhash, algo)
        return classname()
    except AttributeError as err:
        raise ValueError(f'unavailable checksum algorithm: {algo}') from err


def init_xxhash_external_algorithms() -> None:
    '''add external xxhash algorithms to available ALGORITHMS'''

    assert not HAVE_XXHASH_LIB

    # module xxhash is not available;
    # xxhash support via external commands
    for algo in ('xxh32', 'xxh64', 'xxh128'):
        cmd = algo + 'sum'
        try:
            # find 'xxh??sum' command on PATH
            _ = lib.search_path(cmd)
            ALGORITHMS[algo] = ExternalSummer(algo, cmd)
            debug(f'{algo} support via external command: {cmd}')
        except ValueError:
            debug(f'{algo} is not available')


class Summer(SummerInterface):
    '''represents a "checksumming calculator engine"

    A Summer can do one thing: produce a hexdigest for a file

    The Summer uses a `new_func` to create a checksumming instance
    that uses the hashlib API; the obtained instance must implement
    both `.update()` and `.hexdigest()`
    '''

    IOSIZE = 8 * 1024 * 1024

    def __init__(self, algorithm: str, new_func: Callable) -> None:
        '''initialize instance
        `algorithm` is the name of the algorithm
        `new_func` is a function that returns a hashlib-API capable instance
        (for example: `hashlib.new`)
        '''

        self.algorithm = algorithm
        self.new_func = new_func

    def calc(self, filename: str) -> str:
        '''Calculates checksum for a file
        Returns the hexdigest as string
        May raise OSError, ValueError
        '''

        debug(f'checksumming {filename}')
        try:
            # make checksumming object
            summer = self.new_func(self.algorithm)

        except ValueError as err:
            raise ValueError(f'unsupported checksum algorithm: {self.algorithm}') from err

        hexdigest = self.calc_file(summer, filename)
        debug(f'{self.algorithm} {hexdigest} {filename}')
        return hexdigest

    @staticmethod
    def calc_file(summer: Any, filename: str) -> str:
        '''calculate checksum of filename
        `summer` is a hashlib-API capable hashing/checksumming object
        Returns hexdigest string
        May raise OSError
        '''

        # open file
        with open(filename, 'rb') as f:
            # read file data
            while True:
                data = f.read(Summer.IOSIZE)
                if not data:
                    break

                # compute checksum
                summer.update(data)

        return summer.hexdigest()



class ExternalSummer(SummerInterface):
    '''represents a "checksumming calculator engine" that invokes
    an external command to obtain the hexdigest

    The ExternalSummer uses a `cmd` found on PATH to calculate the checksum
    '''

    def __init__(self, algorithm: str, cmd: str) -> None:
        '''initialize instance
        `algorithm` is the name of the algorithm
        `cmd` is an external command present in PATH
        (for example: `xxh64sum`)
        '''

        self.algorithm = algorithm
        self.cmd = cmd

    def calc(self, filename: str) -> str:
        '''Calculates checksum for a file
        Returns the hexdigest as string
        May raise OSError, ValueError
        '''

        debug(f'checksumming {filename}')

        # find command on PATH
        cmdpath = lib.search_path(self.cmd)
        try:
            debug(f'running {cmdpath} {filename}')
            result = subprocess.run([cmdpath, filename], shell=False,
                                    stdout=subprocess.PIPE, stderr=subprocess.DEVNULL,
                                    universal_newlines=True, check=True)
            debug('output == {!r}'.format(result.stdout.strip()))
            lines = result.stdout.split('\n')
            hexdigest, _ = lines[0].split(maxsplit=1)
            # check that it's indeed a hex string
            _ = int(hexdigest, 16)
            debug(f'{self.algorithm} {hexdigest} {filename}')
            return hexdigest

        except (subprocess.CalledProcessError, IndexError, ValueError) as err:
            raise ValueError(f'invalid output from {cmdpath}') from err



class ChecksumError(Exception):
    '''represents a checksum mismatch'''



class Checksum:
    '''represents a checksum of a dmftar volume
    It can load/save dmftar volume .chksum files
    It supports all the available algorithms (given by: `checksum.algorithms()`)
    '''

    def __init__(self, filename: str, algorithm: Optional[str] = None) -> None:
        '''initialise object'''

        self.filename = filename
        # Note that algorithm can be None
        self.algorithm = algorithm
        # 'sum' is the hex digest string
        self.sum = None                 # type: Optional[str]

    def __str__(self) -> str:
        '''Returns string object'''

        return f'{self.algorithm} {self.sum} {os.path.basename(self.filename)}'

    def __eq__(self, other: object) -> bool:
        '''Returns True if checksums match'''

        if not isinstance(other, Checksum):
            # return, not raise (!)
            return NotImplemented

        if self.algorithm is None or other.algorithm is None:
            debug('checksums do not match (algorithm is None)')
            return False

        if self.sum == other.sum and self.algorithm == other.algorithm:
            debug('checksums match')
            return True

        debug('checksums do not match')
        return False

    def __ne__(self, other: object) -> bool:
        '''Returns True if checksums do not match'''

        if not isinstance(other, Checksum):
            # return, not raise (!)
            return NotImplemented

        return not self.__eq__(other)

    def calc(self) -> None:
        '''Calculate checksum of file data
        May raise OSError, ValueError
        '''

        debug(f'checksumming {self.filename}')

        # get summer from map of available algorithms
        try:
            assert self.algorithm
            summer = ALGORITHMS[self.algorithm]
        except KeyError as err:
            raise ValueError(f'unsupported checksum algorithm: {self.algorithm}') from err

        # calculate: this may raise OSError, ValueError
        self.sum = summer.calc(self.filename)
        #debug(f'{self.algorithm} {self.sum} {self.filename}')

    def load(self) -> None:
        '''load checksum file
        May raise OSError, ValueError
        '''

        checksum_file = self.filename + '.chksum'
        debug(f'loading checksum from {checksum_file}')

        with open(checksum_file, encoding='utf-8') as f:
            line = f.readline()
            if not line:
                raise ValueError(f'no data in checksum file: {checksum_file}')

            # strip only the last newline
            # filename field might end with whitespace
            if line[-1] == '\n':
                line = line[:-1]

            if not line:
                raise ValueError(f'invalid checksum file format: {checksum_file}')

            # line should be like "md5 abcdef00 volume"
            arr = line.split(' ', maxsplit=2)
            if len(arr) != 3:
                raise ValueError(f'invalid checksum file format: {checksum_file}')

            algorithm = arr[0]
            # check if the algorithm is known
            if algorithm not in algorithms():
                raise ValueError(f'unsupported checksum algorithm: {algorithm}')

            try:
                chksum = arr[1]
                _ = int(chksum, 16)
            except ValueError as err:
                raise ValueError(f'invalid hexdigest in checksum file: {checksum_file}') from err

            logged_filename = os.path.basename(arr[2])
            if logged_filename != os.path.basename(self.filename):
                warning(f'filename mismatch in checksum file: {checksum_file}')

            self.algorithm = algorithm
            self.sum = chksum

    def save(self) -> None:
        '''save checksum file
        May raise OSError, ValueError
        '''

        checksum_filename = self.filename + '.chksum'
        debug(f'saving checksum as {checksum_filename}')

        if self.sum is None:
            raise ValueError('checksum is None, not saving it')

        with open(checksum_filename, 'w', encoding='utf-8') as f:
            f.write(f'{self}\n')

# EOB
