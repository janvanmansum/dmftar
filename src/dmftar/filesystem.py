#
#   dmftar filesystem.py    WJ116
#   Copyright 2016 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''filesystem functions'''

import os

from typing import Optional, Set

from dmftar.stdio import error, debug

# set([]) of device numbers of filesystems that are DMF capable
# use 'dmf_capable(filename)' to determine whether DMF can be used
DMF_CAPABLE: Optional[Set[int]] = None


def _init() -> None:
    '''gather mount info'''

    global DMF_CAPABLE

    # set of device numbers
    DMF_CAPABLE = set([])

    mtab = '/etc/mtab'
    try:
        with open(mtab, encoding='utf-8') as f:
            for line in f:
                line = line.strip()
                if not line:
                    continue

                arr = line.split()
                if len(arr) < 4:
                    error(f'invalid line in {mtab}: {line}')
                    return

                device = arr[0]
                mountpoint = arr[1]
                fstype = arr[2]
                options = arr[3].split(',')
                debug(f'mountinfo: {device} {mountpoint} {fstype} {arr[3]}')
                dmf_cap = False
                if 'dmapi' in options:
                    debug(f'{mountpoint} is mounted with dmapi')
                    # DMF utils can be used
                    dmf_cap = True

                if fstype.startswith('nfs') and ':/cxfs/archive' in device:
                    debug(f'{mountpoint} is an NFS mounted /cxfs/archive filesystem')
                    # dmf-utils can be used
                    dmf_cap = True

                if dmf_cap:
                    # get the device number by stat()ing the mountpoint
                    # Note that we could also stat the device path
                    # and examine st_rdev
                    try:
                        statbuf = os.stat(mountpoint)
                    except OSError as err:
                        debug(f'stat({mountpoint}): {err.strerror}')
                    else:
                        debug(f'DMF_CAPABLE.add({statbuf.st_dev})')
                        DMF_CAPABLE.add(statbuf.st_dev)

    except FileNotFoundError as err:
        # some OS (e.g. macOS) do not have mtab
        # since these systems are typically not dmf-capable,
        # do not bother the user with any error/warning
        debug(f'failed to open {mtab}: {err.strerror}')
        return

    except OSError as err:
        error(f'read error: {mtab}: {err.strerror}')


def dmf_capable(path: str) -> bool:
    '''Returns True if filesystem for path is DMF capable'''

    if DMF_CAPABLE is None:
        _init()

    try:
        statbuf = os.stat(path)
    except OSError as err:
        debug(f'stat({path}): {err.strerror}')
        #debug(f'{path}: False')
        return False

    assert DMF_CAPABLE is not None          # this helps mypy
    if statbuf.st_dev in DMF_CAPABLE:
        #debug(f'{path}: True')
        return True

    #debug(f'{path}: False')
    return False


# unit test
if __name__ == '__main__':
    # pylint complains: import should be in top of file
    # pylint: disable=I0011,C0413
    import dmftar.stdio
    dmftar.stdio.DEBUG = True
    dmf_capable('/')
    dmf_capable('/usr/people/walter/.bashrc')
    dmf_capable('/archive/sscpjong/test1g.dat')
    dmf_capable('unknown-file')
    dmf_capable('/archive/sscpjong/unknown-file')
    dmf_capable('/backup/dmf_test/test01.dat')


# EOB
