#
#   dmftar interactive.py    WJ116
#   Copyright 2016 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''interactive shell-like command-line interface for browsing
dmftar archives. Can also interactively select and extract
a subset of files
'''

import os
import getopt
import shlex
import struct
import fcntl
import termios
import fnmatch
import readline
import tempfile
import subprocess
import errno
import time

from typing import List, Dict, Tuple, Callable, Iterator, Optional

from dmftar.stdio import error, debug
from dmftar.lib import ExecError, SilentError
from dmftar.index import IndexEntry, unescape, is_gnu_fileparts
from dmftar.lrucache import LRUCache

# only used for typing:
import dmftar.archive

# DirEntry objects
ROOT = None     # type: Optional[DirEntry]
CWD = None      # type: Optional[DirEntry]

# cached directory DirEntry entries
MAX_DCACHE = 10000
DCACHE = LRUCache(MAX_DCACHE)

# cached file DirEntry entries
MAX_FCACHE = 100000
FCACHE = LRUCache(MAX_FCACHE)

# use to format columns for 'ls'
# term width is detected
TERM_WIDTH = 80

# output buffer
# particularly used for piping output through an external command
OUTPUT = []                                 # type: List[str]
REDIRECTED = False

# dict {path:[filenames, ..],} of files to extract
EXTRACT: Dict[str, List[str]] = {}

# option flags for command 'ls'
OPT_LS_SINGLE = 1
OPT_LS_ALL = 2
OPT_LS_LONG = 4
OPT_LS_DIRECTORY = 8


class DirEntryError(Exception):
    '''error related to DirEntry'''



class DirEntry(IndexEntry):
    '''represents a single entry in a dmftar index'''

    # pylint: disable=no-member,attribute-defined-outside-init

    def __init__(self, index_number: int, offset: int) -> None:
        '''initialize'''

        super().__init__()

        self.index = index_number
        self.offset = offset

        # reference to .. DirEntry
        self.parent = None                  # type: Optional[DirEntry]

        # directory contents; list of fnames
        self.dirlist = None                 # type: Optional[List[str]]

        # fullpath is just an alias for path
        # because all the interactive code uses "fullpath"
        self.fullpath = self.path

    def parse(self, line: str, init_unescape: bool = False, init_mode: bool = False) -> None:
        '''fill in stat info from dmftar index line
        May raise ValueError
        '''

        super().parse(line, init_unescape, init_mode)

        # make all paths start with a slash
        # The path resolving code relies on it
        assert self.path
        if self.path[0] != '/':
            self.path = '/' + self.path
        # fullpath is just an alias for path
        self.fullpath = self.path

    def copy(self) -> 'DirEntry':
        '''Returns copy of self'''

        d = DirEntry(self.index, self.offset)
        d.mode = self.mode
        d.owner = self.owner
        d.group = self.group
        d.size = self.size
        d.mtime = self.mtime
        d.path = self.path
        d.name = self.name
        d.parent = self.parent
        d.dirlist = self.dirlist
        d.fullpath = d.path
        d.linkdest = self.linkdest
        return d

    def __repr__(self) -> str:
        '''returns string representation'''

        name = self.name
        if self.isdir():
            isdir = 'DIR'
            name += '/'
        else:
            isdir = '-'
        return f'<DirEntry>({name},{self.index},{self.offset},{isdir})'



class Completer:
    '''GNU readline tab-completion'''

    def __init__(self, archive: dmftar.archive.Archive) -> None:
        '''initialize instance'''

        self.archive = archive
        self.matches = []                   # type: List[str]

    def complete(self, text: str, state: int) -> Optional[str]:
        '''complete given text'''

        debug(f'text: [{text}]  state:{state}')

        if state == 0:
            # first time
            path = dirname = os.path.dirname(text)
            if not path:
                path = '.'
            name = os.path.basename(text)
            debug(f'text:[{text}]  path:[{path}]  name:[{name}]')

            try:
                d = resolv_path(self.archive, path)
            except DirEntryError:
                # error resolving this path; no matches
                return None
            except ValueError as err:
                error(str(err))
                return None
            except OSError as err:
                error(f'{err.filename}: {err.strerror}')
                return None

            self.matches = []
            dirlist = listdir_archive(self.archive, d)
            for entry in dirlist:
                if entry.name in ('.', '..'):
                    continue

                match = None
                if name:
                    if entry.name.startswith(name):
                        if dirname:
                            match = os.path.join(dirname, entry.name)
                        else:
                            match = entry.name

                else:
                    if dirname:
                        match = os.path.join(dirname, entry.name)
                    else:
                        match = entry.name

                if match is not None:
                    if entry.isdir():
                        match += '/'
                    self.matches.append(match)

            debug('matches == [')
            for x in self.matches:
                debug(f'  {x}')
            debug(']')

        # other state code
        try:
            return self.matches[state]
        except IndexError:
            return None



def terminal_size() -> Tuple[int, int]:
    '''Returns pair: width, height of terminal window'''

    if not os.isatty(0):
        return 80, 25

    params = struct.pack('HHHH', 0, 0, 0, 0)
    size_struct = fcntl.ioctl(0, termios.TIOCGWINSZ, params)
    h, w, _, _ = struct.unpack('HHHH', size_struct)
    debug(f'width == {w}  height == {h}')
    return w, h


def out(line: str) -> None:
    '''output a line of text'''

    OUTPUT.append(line)


def flush() -> None:
    '''flush pending output'''

    global OUTPUT

    for line in OUTPUT:
        print(line)
    OUTPUT = []


def open_archive(archive: dmftar.archive.Archive) -> None:
    '''Initializes ROOT and CWD
    May raise OSError, ValueError
    '''

    global ROOT, CWD, DCACHE, FCACHE

    # interactive mode works only locally anyway
    assert archive.remote is None

    # issue dmget for all index files
    # make list of volumes to get index for
    idx_volbatch = [archive.volume_bynumber(x)
                    for x in range(1, archive.num_volumes + 1)]
    archive.dmget_volume_batch(idx_volbatch, index_only=True)

    # the root dir must be present in the first volume
    vol = archive.volume_bynumber(1)
    index_filename = f'{vol}.idx'
    debug('index_filename == ' + index_filename)
    offset = 0
    try:
        with open(index_filename, encoding='utf-8') as f:
            # root dir is the first entry, really
            line = f.readline()
            offset = f.tell()
    except OSError as err:
        raise OSError(err.errno, err.strerror, index_filename) from err

    entry = DirEntry(1, offset)
    try:
        entry.parse(line)
    except ValueError as err:
        raise ValueError(f'{index_filename}: {err}') from err

    # circular reference; parent of root dir is the root
    entry.parent = entry

    if entry.isdir():
        ROOT = CWD = entry
    else:
        # first entry is not a directory
        # make a new root DirEntry
        ROOT = CWD = make_root(entry)
    debug(f'ROOT == [{ROOT.fullpath}]')

    # reset caches
    DCACHE = LRUCache(MAX_DCACHE)
    FCACHE = LRUCache(MAX_FCACHE)


def make_root(entry: DirEntry) -> DirEntry:
    '''make directory DirEntry object from entry
    Returns new DirEntry object
    '''

    root = entry.copy()
    root.index = 1
    root.offset = 0
    root.mode = 'd' + root.mode[1:]
    root.size = 0
    root.fullpath = os.path.dirname(root.fullpath)
    root.name = ''
    return root


def make_dot(entry: DirEntry) -> DirEntry:
    '''make . directory DirEntry object from entry
    Returns new DirEntry object
    '''

    debug(f'dot directory points at: {entry.fullpath}')
    d = entry.copy()
    d.name = '.'
    return d


def make_dotdot(entry: DirEntry) -> DirEntry:
    '''make .. directory DirEntry object for entry
    Returns new DirEntry object
    '''

    assert entry.parent is not None
    debug(f'dotdot directory points at: {entry.parent.fullpath}')
    parent = entry.parent.copy()
    parent.name = '..'
    return parent


def listdir_cache(entry: DirEntry) -> Tuple[List[DirEntry], bool]:
    '''list directory at entry using only the cache
    Returns tuple: (dirlist, cache_hit)

    cache_hit is False on cache miss, in which case the returned dirlist
    will be truncated
    '''

    assert entry.dirlist is not None

    dirlist = [make_dot(entry), make_dotdot(entry), ]

    # we already have the list of names
    for fname in entry.dirlist:
        fullpath = os.path.join(entry.fullpath, fname)
        try:
            d = cache_lookup(fullpath)
            dirlist.append(d)
        except KeyError:
            return dirlist, False

    debug(f'cache hit: contents of {entry.fullpath}')
    return dirlist, True


def listdir_archive(archive: dmftar.archive.Archive, entry: DirEntry) -> List[DirEntry]:
    '''list directory at entry
    Returns list of DirEntry objects
    '''

    assert entry is not None
    assert entry.isdir()
    assert entry.index <= archive.num_volumes

    if entry.dirlist is not None:
        # try loading it from cache
        dirlist, ok = listdir_cache(entry)
        if ok:
            return dirlist

    dirlist = [make_dot(entry), make_dotdot(entry), ]

    # load directory from disk
    fnames = []     # type: List[str]

    # entry is a directory; we add a trailing slash
    # the directory 'level' of the path is how deep it is;
    # (count the number of slashes)
    # when going through the index we use this as a trick to speed up
    # finding all files in this directory
    path = entry.fullpath + '/'
    level = path.count('/')
    path_len = len(path)
    debug(f'{level} {path_len} [{path}]')

    offset = entry.offset

    volume_number = entry.index
    while volume_number <= archive.num_volumes:
        vol = archive.volume_bynumber(volume_number)
        index_filename = f'{vol}.idx'
        debug(f'index_filename == {index_filename}')
        try:
            with open(index_filename, encoding='utf-8') as f:
                if offset:
                    # only for first volume encountered
                    f.seek(offset, os.SEEK_SET)
                    offset = 0

                lineno = 0

                while True:
                    lineno += 1
                    line = f.readline()
                    if not line:
                        break

                    try:
                        s_mode, _, _, _, _, fname, _ = IndexEntry.split_line(line)
                    except ValueError as err:
                        error(f'index #{volume_number}: {err}')
                        # break out loop and continue with next volume
                        break

                    # Note: fname may be escaped
                    # dmftar will display escaped filenames
                    # and we don't unescape() until really needed

                    if lineno == 1:
                        # check for continued multi-volume header
                        if s_mode[0] == 'M':
                            # the 'M------' volume header bits for
                            # old-style GNU tar header
                            debug('continued old-style multi-volume header')
                            continue

                        if s_mode[0] == '-' and is_gnu_fileparts(fname):
                            # new style GNU tar multi-volume header has
                            # a "virtual" subdir named 'GNUFileParts.<pid>'
                            debug('continued new-style multi-volume header')
                            continue

                    # count slashes + 1 because path has a leading slash (for root)
                    # that is not actually present in the index
                    dirlevel = fname.count('/') + 1     # len(fname.split(os.sep))
                    if dirlevel > level:
                        # deeper dir; not the same dir
                        continue

                    if dirlevel < level:
                        # d is not under directory;
                        # finished scanning this directory
                        entry.dirlist = fnames
                        # return list of DirEntry objects
                        return dirlist

                    d = DirEntry(volume_number, f.tell())
                    try:
                        d.parse(line)
                    except ValueError as err:
                        error(f'{index_filename}: {err}')
                        continue

                    d.parent = entry

                    fnames.append(os.path.basename(d.fullpath))

#                    debug(f'append {d}')
                    dirlist.append(d)
                    add_cache(d)

        except OSError as err:
            # report the error, but do not break out
            error(f'{index_filename}: {err.strerror}')
            # continue with next volume

        # directory may continue in next volume
        volume_number += 1

    # reached end of index
    entry.dirlist = fnames
    # return list of DirEntry objects
    return dirlist


def find_dentry(archive: dmftar.archive.Archive, parent: DirEntry, name: str) -> DirEntry:
    '''Returns DirEntry instance of name under parent dir
    May raise DirEntryError, ValueError, OSError

    DirEntryError: no such file or directory
    ValueError: index parsing error
    OSError: filesystem problems, permission denied, memory pressure
    '''

    assert parent is not None
    assert parent.isdir()
    assert parent.index <= archive.num_volumes

    if name == '.':
        return parent

    # this case should be handled at a higher level
    assert name != '..'

    assert name[0] != '/'
    search_fname = os.path.join(parent.fullpath, name)

    try:
        return cache_lookup(search_fname)
    except KeyError:
        pass

    if parent.dirlist is not None and name not in parent.dirlist:
        # quick exit: no such file or directory
        raise DirEntryError('No such file or directory')

    # entry is not cached; go find it in the index file(s)
    # strip leading slash because index paths don't have those
    if search_fname[0] == '/':
        search_fname = search_fname[1:]

    level = search_fname.count('/')

    offset = parent.offset

    volume_number = parent.index
    while volume_number <= archive.num_volumes:
        vol = archive.volume_bynumber(volume_number)
        index_filename = f'{vol}.idx'
        debug(f'index_filename == {index_filename}')
        try:
            with open(index_filename, encoding='utf-8') as f:
                if offset:
                    # only for first volume encountered
                    f.seek(offset, os.SEEK_SET)
                    offset = 0

                lineno = 0

                while True:
                    lineno += 1
                    line = f.readline()
                    if not line:
                        break

                    try:
                        s_mode, _, _, _, _, fname, _ = IndexEntry.split_line(line)
                    except ValueError as err:
                        raise ValueError(f'{index_filename}: {err}') from err

                    if lineno == 1:
                        # check for continued multi-volume header
                        if s_mode[0] == 'M':
                            # the 'M------' volume header bits for
                            # old-style GNU tar header
                            debug('continued old-style multi-volume header')
                            continue

                        if s_mode[0] == '-' and is_gnu_fileparts(fname):
                            # new style GNU tar multi-volume header has
                            # a "virtual" subdir named 'GNUFileParts.<pid>'
                            debug('continued new-style multi-volume header')
                            continue

                    dirlevel = fname.count('/')
                    if dirlevel > level:
                        # deeper dir; not the same dir
                        # keep scanning until we find our dirlevel again
                        continue

                    if dirlevel < level:
                        # not in this directory
                        raise DirEntryError('No such file or directory')

                    if fname == search_fname:
                        # found it
                        d = DirEntry(volume_number, f.tell())
                        try:
                            d.parse(line)
                        except ValueError as err:
                            raise ValueError(f'{index_filename}: {err}') from err

                        d.parent = parent

                        debug(f'found dentry: {d}')
                        add_cache(d)
                        # if it is a directory, we do not read the contents
                        # at this point;
                        # reading the directory is done in listdir_archive()
                        return d

        except OSError as err:
            raise OSError(err.errno, err.strerror, index_filename) from err

        # directory may continue in next volume
        volume_number += 1

    # no such file or directory
    raise DirEntryError('No such file or directory')


def add_cache(entry: DirEntry) -> None:
    '''add DirEntry to cache'''

    if entry.isdir():
        DCACHE[entry.fullpath] = entry
    else:
        FCACHE[entry.fullpath] = entry


def cache_lookup(fullpath: str) -> DirEntry:
    '''Returns DirEntry for fullpath
    Raises KeyError on cache miss
    '''

    try:
        entry = DCACHE[fullpath]
        debug(f'cache hit: {fullpath}')
        return entry
    except KeyError:
        pass

    try:
        entry = FCACHE[fullpath]
        debug(f'cache hit: {fullpath}')
        return entry
    except KeyError:
        pass

    debug(f'cache miss: {fullpath}')
    raise KeyError


def resolv_path(archive: dmftar.archive.Archive, path: str) -> DirEntry:
    '''Returns DirEntry to path
    path may be an absolute or a relative path
    This function does not do globbing

    May raise DirEntryError, ValueError, OSError
    '''

    debug(f'path: [{path}]')
    assert CWD is not None
    debug(f' CWD: [{CWD.fullpath}]')
    assert ROOT is not None
    debug(f'ROOT: [{ROOT.fullpath}]')

    # "if not path" really depends on what it is you want to do;
    # and should be handled by the calling code, not here
    assert path

    # collapse double slashes
    while '//' in path:
        path = path.replace('//', '/')

    # strip trailing slash
    if path[-1] == '/':
        path = path[:-1]

    if not path:
        return ROOT

    try:
        return cache_lookup(path)
    except KeyError:
        pass

    elems = path.split('/')
    if not elems[0]:
        # absolute path; start at root
        d = ROOT
        elems.pop(0)
        debug(f'absolute path; d == ROOT == [{d.fullpath}]')
    else:
        # relative path; start at CWD
        d = CWD
        debug(f'relative path; d == [{d.fullpath}]')

    for elem in elems:
        debug(f'path elem == [{elem}]')

        if not d.isdir():
            raise DirEntryError(f'{d.fullpath}: Not a directory')

        if elem == '.':
            continue

        if elem == '..':
            assert d.parent is not None
            d = d.parent
            continue

        d = find_dentry(archive, d, elem)

    return d


def get_cwd() -> str:
    '''returns current fullpath'''

    assert CWD is not None
    assert CWD.isdir()
    return CWD.fullpath


def cmd_help() -> None:
    '''show help information'''

    out('''
You are running dmftar in interactive mode. This provides a
shell-like environment which allows for interactively browsing
dmftar archives. Available commands are:

  help          Show this information
  ls            Show directory listing
  cd            Change directory
  pwd           Show current directory
  add           Add entries to extraction list
  remove        Remove entries from extraction list
  show          Display the extraction list
  extract       Extract selected files and directories
  exit, quit    Exit the dmftar shell


dmftar shell can pipe command output, for example:

  ls -l |more
''')


def usage_cd() -> None:
    '''show usage for the 'cd' command'''

    out('''usage: cd [DIRECTORY]

Changes the current directory.
''')


def cmd_cd(archive: dmftar.archive.Archive, argv: List[str]) -> None:
    '''change directory command'''

    global CWD

    if not argv:
        arg = None
    else:
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except getopt.GetoptError as err:
            error(f'cd: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_cd()
                return

        if not args:
            arg = None
        else:
            # for the 'cd' command, only the first argument matters
            # any other arguments are bluntly ignored
            arg = args[0]

    if not arg:
        # go to the root dir
        entry = ROOT
    else:
        try:
            entry = resolv_path(archive, arg)
        except DirEntryError as err:
            error(f'{arg}: {err}')
            return

    assert entry is not None
    if not entry.isdir():
        error(f'{arg}: Not a directory')
        return

    CWD = entry


def cmd_pwd() -> None:
    '''print current working directory'''

    out(get_cwd())


def usage_ls() -> None:
    '''show usage for the 'ls' command'''

    out('''usage: ls [options] [pattern ...]
options:
  -h, --help        Show this information
  -1, --single      Output a single column
  -a, --all         Also show hidden files
  -l, --long        Use a long listing format
  -d, --directory   list directory entries instead of contents

Files shown with an asterisk ('*') are marked for extraction.
''')


def cmd_ls(archive: dmftar.archive.Archive, argv: List[str]) -> None:
    '''show the listing'''

    options = 0
    args = []   # type: List[str]
    if argv:
        try:
            opts, args = getopt.getopt(argv, 'h1ald',
                                       ['help', 'single', 'all', 'long',
                                        'directory'])
        except getopt.GetoptError as err:
            error(f'ls: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_ls()
                return

            if opt in ('-1', '--single'):
                options |= OPT_LS_SINGLE

            elif opt in ('-a', '--all'):
                options |= OPT_LS_ALL

            elif opt in ('-l', '--long'):
                options |= OPT_LS_LONG

            elif opt in ('-d', '--directory'):
                options |= OPT_LS_DIRECTORY

    if not args:
        args = ['.', ]

    # the following code is adapted from cdi-dmf-utils/dmls.py
    # which is an excellent 'ls' implementation in Python

    # used for printing newlines
    first = True

    files = []
    dirs = []
    for arg in args:
        try:
            entry = resolv_path(archive, arg)
        except DirEntryError as err:
            error(f'{arg}: {err}')
            continue

        if entry.isdir():
            if options & OPT_LS_DIRECTORY:
                # do not load directory contents
                files.append(entry)
            else:
                dirs.append(entry)
        else:
            files.append(entry)

    if files:
        first = False
        listdir_files(files, options)

    if dirs:
        if not first:
            out('')
        else:
            first = False

        num_dirs = len(dirs)
        n = 0
        for entry in dirs:
            if len(args) > 1:
                out(f'{entry.fullpath}:')

            listdir_content(archive, entry, options)
            n += 1
            if n < num_dirs:
                out('')


def listdir_content(archive: dmftar.archive.Archive, entry: DirEntry, options: int) -> None:
    '''Show directory listing'''

    assert entry is not None
    debug(f'entry == {entry}')
    debug(f'entry.fullpath == {entry.fullpath}')

    # get directory listing
    dirlist = listdir_archive(archive, entry)

    if not options & OPT_LS_ALL:
        # filter hidden files
        dirlist = [x for x in dirlist if x.name[0] != '.']

    # show listing
    listdir_files(dirlist, options)


def listdir_files(files: List[DirEntry], options: int) -> None:
    '''Show listing for individual files'''

    if not files:
        return

    # sort by name
    files = sorted(files, key=lambda x: x.name)

    if options & OPT_LS_LONG:
        listdir_files_long(files, options)
    else:
        listdir_files_short(files, options)


def listdir_files_long(files: List[DirEntry], _options: int) -> None:
    '''Show long listing for individual files'''

    # max width of owner/group
    w_owner = max((len(x.owner) + len(x.group)) for x in files) + 2

    # max width of size
    w_size = max(len(f'{x.size}') for x in files) + 2
    w_size = max(7, w_size)

    for entry in files:
        name = display_name(entry)

        # display owner and group together
        owner = f'{entry.owner}/{entry.group}'

        s_date = time.strftime('%Y-%m-%d', entry.mtime)
        s_time = time.strftime('%H:%M', entry.mtime)

        if entry.linkdest is not None:
            out(f'{entry.mode} {owner:<{w_owner}} {entry.size:{w_size}} {s_date} {s_time} {name} -> {entry.linkdest}')
        else:
            out(f'{entry.mode} {owner:<{w_owner}} {entry.size:{w_size}} {s_date} {s_time} {name}')


def display_name(entry: DirEntry) -> str:
    '''Returns name as displayed by 'ls'
    Names in the extraction list are marked with an asterisk
    Directories get a trailing slash
    '''

    # mark files on extraction list with asterisk
    path = os.path.dirname(entry.fullpath)
    name = os.path.basename(entry.fullpath)
    try:
        if name in EXTRACT[path]:
            name = '*' + entry.name
        else:
            name = ' ' + entry.name
    except KeyError:
        name = ' ' + entry.name

    if entry.isdir():
        name += '/'

    return name


def listdir_files_short(files: List[DirEntry], options: int) -> None:
    '''Show short listing for individual files'''

    if (options & OPT_LS_SINGLE) or REDIRECTED:
        for entry in files:
            name = display_name(entry)
            out(name)
        return

    # list with variable width columns
    num_cols, column_widths = calculate_columns(files)
    num_rows = len(files) // num_cols
    if len(files) % num_cols > 0:
        num_rows += 1

    debug(f'num_cols == {num_cols}  num_rows == {num_rows}')

    out_line = ''

    # list vertically sorted columns
    for row in range(num_rows):
        idx = row
        col = 0
        while col < num_cols:
            name = display_name(files[idx])

            width = min(column_widths[col], TERM_WIDTH - 1)
            out_line += f'{name:<{width}}'

            idx += num_rows
            if idx >= len(files):
                break
            col += 1

        out(out_line)
        out_line = ''


def calculate_columns(files: List[DirEntry]) -> Tuple[int, List[int]]:
    '''calculate optimal number of columns
    Returns number of columns, list of column widths
    '''

    # Note: this is nearly the same as in dmls.py in cdi-rdmf-utils

    max_cols = min(TERM_WIDTH // 3, len(files))
    debug(f'max_cols == {max_cols}')

    line_len = [0 for _ in range(max_cols)]
    col_arr = [[0 for _z in range(max_cols)]
               for _ in range(max_cols)]
    valid_len = [True for _ in range(max_cols)]

    fn = 0
    for entry in files:
        # length + 1 because file may be marked with '*'
        name_len = len(entry.name) + 1
        if entry.isdir():
            # append slash (virtually)
            name_len += 1

        for col in range(max_cols):
            if valid_len[col]:
                # idx is placement, in which column
                # the file appears on screen
                idx = fn // ((len(files) + col) // (col + 1))

                display_len = name_len
                if idx != col:
                    display_len += 2

                if col_arr[col][idx] < display_len:
                    line_len[col] += (display_len - col_arr[col][idx])
                    col_arr[col][idx] = display_len
                    valid_len[col] = line_len[col] < TERM_WIDTH
        fn += 1

    col = max_cols - 1
    while col >= 0:
        if valid_len[col]:
            debug(f'return {col + 1}, {col_arr[col]}')
            return col + 1, col_arr[col]
        col -= 1

    # this line is never reached
    debug(f'never reached! return 1, {col_arr[0]}')
    return 1, col_arr[0]


def usage_add() -> None:
    '''show usage for the 'add' command'''

    out('''usage: add [pattern ...]

Adds file(s) or dir(s) to an internal list of entries to extract
from the dmftar archive. A file globbing pattern (with wildcards
such as '*' and '?') may be given.

Added files are shown with an asterisk ('*') by the 'ls' command.

Related commands:
  remove        Remove entries from extraction list
  show          Display the extraction list
  extract       Extract selected files and directories
''')


def cmd_add(archive: dmftar.archive.Archive, argv: List[str]) -> None:
    '''add file(s) or dir(s) to list of files to extract'''

    args = []   # type: List[str]
    if argv:
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except getopt.GetoptError as err:
            error(f'add: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_add()
                return

    if not args:
        out('usage: add [-h] [pattern ...]')
        return

    # add files to extraction list
    for arg in args:
        try:
            entry = resolv_path(archive, arg)
        except DirEntryError as err:
            error(f'{arg}: {err}')
            continue

        path = os.path.dirname(entry.fullpath)
        # extraction list is a dict by directory (path)
        # each dict member is a list of dir entries (only names)
        try:
            dir_contents = EXTRACT[path]
        except KeyError:
            dir_contents = []
            EXTRACT[path] = dir_contents

        if entry.name not in dir_contents:
            dir_contents.append(entry.name)


def usage_remove() -> None:
    '''show usage for the 'remove' command'''

    out('''usage: remove [pattern ...]

Removes entries from the list of files to extract.

Related commands:
  add           Add entries to extraction list
  show          Display the extraction list
  extract       Extract selected files and directories
''')


def cmd_remove(archive: dmftar.archive.Archive, argv: List[str]) -> None:
    '''remove entries from extraction list'''

    args = []   # type: List[str]
    if argv:
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except getopt.GetoptError as err:
            error(f'remove: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_remove()
                return

    if not args:
        out('usage: remove [-h] [pattern ...]')
        return

    for arg in args:
        try:
            entry = resolv_path(archive, arg)
        except (DirEntryError, ValueError) as err:
            error(f'{arg}: {err}')
            continue
        except OSError as err:
            error(f'{arg}: {err.filename}: {err.strerror}')
            continue

        path = os.path.dirname(entry.fullpath)
        try:
            arr = EXTRACT[path]
        except KeyError:
            continue

        if entry.name in arr:
            arr.remove(entry.name)

            # remove empty lists
            if not arr:
                del EXTRACT[path]


def usage_show() -> None:
    '''show usage for the 'show' command'''

    out('''usage: show

Shows list of files currently marked for extraction.

Related commands:
  add           Add entries to extraction list
  remove        Remove entries from extraction list
  extract       Extract selected files and directories
''')


def cmd_show(argv: List[str]) -> None:
    '''show extraction list'''

    args = []   # type: List[str]
    if argv:
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except getopt.GetoptError as err:
            error(f'show: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_show()
                return

    if args:
        out('usage: show [-h]')
        return

    dirlist = EXTRACT.keys()
    if not dirlist:
        out('no files selected for extraction yet')
        return

    dirlist = sorted(dirlist)   # type: ignore # dirlist is an iterable
    for path in dirlist:
        arr = EXTRACT[path]

        # print fullpath if it fits in the terminal
        # else print a shorter version
        fullpaths = [os.path.join(path, x) for x in arr]
        if not fullpaths:
            # empty list
            continue

        max_width = max(len(x) for x in fullpaths)
        if max_width < TERM_WIDTH:
            for fullpath in sorted(fullpaths):
                out(fullpath)
        else:
            out(path)
            for name in sorted(arr):
                out(f'  {name}')


def usage_extract() -> None:
    '''show usage for the 'show' command'''

    out('''usage: extract

Extract previously selected files from dmftar archive.

Related commands:
  add           Add entries to extraction list
  remove        Remove entries from extraction list
  show          Display the extraction list
''')


def cmd_extract(arch: dmftar.archive.Archive, argv: List[str]) -> None:
    '''extract extraction list
    May raise ExecError
    '''

    global EXTRACT

    args = []   # type: List[str]
    if argv:
        try:
            opts, args = getopt.getopt(argv, 'h', ['help'])
        except getopt.GetoptError as err:
            error(f'extract: {err}')
            return

        for opt, _optarg in opts:
            if opt in ('-h', '--help'):
                usage_extract()
                return

    if args:
        out('usage: extract [-h]')
        return

    if not EXTRACT:
        out('no files selected for extraction yet')
        return

    # write filenames to extract to a tempfile
    # the filenames end with a null byte
    with tempfile.NamedTemporaryFile(prefix='dmftar_', suffix='.tmp') as f:
        for dirname, dir_contents in EXTRACT.items():
            for name in dir_contents:
                fullpath = os.path.join(dirname, name)
                if fullpath[0] == '/':
                    # strip leading slash
                    fullpath = fullpath[1:]

                    # these filenames are escaped
                    # so unescape them for tar --extract
                    fullpath = unescape(fullpath)

                debug(f'extract fullpath [{fullpath}]')
                data = (fullpath + '\0').encode()
                f.write(data)
        f.flush()

        # execute dmftar itself to extract the filelist

        # manipulate archive object config for extraction
        arch.tar_subcommand = '-x'
        arch.config.from_file = f.name
        arch.config.opt_null = True

        # clear out any TAR_xxx vars that may be there already
        # they are not supposed to be set at this moment
        arch.cleanup_environment()

        # extract the file list
        try:
            arch.extract()
        except ExecError as err:
            # re-raise error
            # this will happen after the _finally_ clause executed
            raise err
        finally:
            # reset config back to interactive mode
            arch.tar_subcommand = '--interactive'
            arch.config.from_file = None
            arch.config.opt_null = False

            # leaving the 'with' context
            debug(f'auto-removing tempfile {f.name}')

    # reset extraction list
    EXTRACT = {}


def split_pipe(line: str) -> Tuple[str, Optional[str]]:
    '''splits line into command, pipe command
    May raise ValueError on invalid syntax
    '''

    if '|' not in line:
        return line, None

    pos = 0
    escape = False
    in_quote = False
    in_double_quote = False
    for c in line:
        if escape:
            # accept any character
            escape = False
        elif c == '\\':
            escape = True
            pos += 1
            continue

        elif c == "'" and not in_double_quote:
            in_quote = not in_quote
        elif c == '"' and not in_quote:
            in_double_quote = not in_double_quote
        elif c == '|' and not in_quote and not in_double_quote:
            cmd = line[:pos]
            cmd = cmd.strip()
            if not cmd:
                raise ValueError('no command given')

            pipe_cmd = line[pos + 1:]
            pipe_cmd = pipe_cmd.strip()
            if not pipe_cmd:
                raise ValueError('no pipe command given')

            return cmd, pipe_cmd

        elif c == '<':
            raise ValueError('input redirection is not supported')
        elif c == '>':
            # well ... maybe not too difficult to actually support this
            raise ValueError('output redirection is not supported')
        pos += 1
        escape = False

    if in_quote or in_double_quote:
        # like shlex, raise ValueError
        raise ValueError('No closing quotation')

    return line, None


def glob_path_exists(archive: dmftar.archive.Archive, path: str) -> bool:
    '''Returns True if path exists'''

    assert ROOT is not None
    root_path = ROOT.fullpath + '/'
    root_len = len(ROOT.fullpath)

    if path.startswith(root_path):
        path = path[root_len:]

    try:
        _ = resolv_path(archive, path)
    except DirEntryError:
        return False
    except ValueError as err:
        error(str(err))
        return False
    except OSError as err:
        error(f'{err.filename}: {err.strerror}')
        return False

    return True


def glob_path_isdir(archive: dmftar.archive.Archive, path: str) -> bool:
    '''Returns True if path is a directory'''

    assert ROOT is not None
    root_path = ROOT.fullpath + '/'
    root_len = len(ROOT.fullpath)

    if path.startswith(root_path):
        path = path[root_len:]

    try:
        entry = resolv_path(archive, path)
    except DirEntryError:
        return False
    except ValueError as err:
        error(str(err))
        return False
    except OSError as err:
        error(f'{err.filename}: {err.strerror}')
        return False

    return entry.isdir()


def glob_path_listdir(archive: dmftar.archive.Archive, path: str) -> List[str]:
    '''Returns list of names in path'''

    assert ROOT is not None
    root_path = ROOT.fullpath + '/'
    root_len = len(ROOT.fullpath)

    if path.startswith(root_path):
        path = path[root_len:]

    try:
        entry = resolv_path(archive, path)
    except DirEntryError as err:
        debug(f'{path}: {err}')
        return []
    except ValueError as err:
        error(str(err))
        return []
    except OSError as err:
        error(f'{err.filename}: {err.strerror}')
        return []

    if not entry.isdir():
        debug(f'{path}: Not a directory')
        return []

    entries = listdir_archive(archive, entry)
    names = [x.name for x in entries]
    debug(f'return {names}')
    return names


# the globbing code was adapted from Python's own module glob
# but made to work for dmftar index

def glob(archive: dmftar.archive.Archive, pattern: str) -> List[str]:
    '''return list of paths matching pattern
    Pattern may contain shell-style wildcards
    '''

    debug(f'pattern == {pattern}')
    return list(_iglob(archive, pattern))


def _iglob(archive: dmftar.archive.Archive, pathname: str) -> Iterator[str]:
    '''return iterator which yields matching paths'''

    dirname, filename = os.path.split(pathname)
    debug(f'dirname: [{dirname}]  filename: [{filename}]')
    if not is_pattern(pathname):
        if filename:
            if glob_path_exists(archive, pathname):
                yield pathname
        else:
            # no filename; must be a directory
            if glob_path_isdir(archive, dirname):
                yield pathname
        return

    if dirname != pathname and is_pattern(dirname):
        dirs = _iglob(archive, dirname)
    else:
        # Redefinition of dirs type from generator to list
        # which is entirely OK in this context
        dirs = [dirname, ]      # type: ignore  # type: List[str]

    glob_in_dir_func: Optional[Callable[[dmftar.archive.Archive, str, str], List[str]]] = None
    if is_pattern(filename):
        glob_in_dir_func = _glob1
    else:
        glob_in_dir_func = _glob0

    for dirname in dirs:
        for name in glob_in_dir_func(archive, dirname, filename):
            yield os.path.join(dirname, name)


def _glob1(archive: dmftar.archive.Archive, dirname: str, pattern: str) -> List[str]:
    '''returns list of matching filenames in directory'''

    if not dirname:
        dirname = get_cwd()

    names = glob_path_listdir(archive, dirname)

    if pattern[0] != '.':
        # filter hidden entries
        names = [x for x in names if x[0] != '.']

    return fnmatch.filter(names, pattern)


def _glob0(archive: dmftar.archive.Archive, dirname: str, filename: str) -> List[str]:
    '''return filename as list if it exists'''

    if not filename:
        if glob_path_isdir(archive, dirname):
            return ['', ]
    else:
        if glob_path_exists(archive, os.path.join(dirname, filename)):
            return [filename, ]
    return []


def is_pattern(pattern: str) -> bool:
    '''Returns True if argument looks like a file matching pattern'''

    if '*' in pattern or '?' in pattern:
        return True

    idx1 = pattern.find('[')
    if idx1 >= 0:
        idx2 = pattern.rfind(']')
        if idx2 > idx1:
            return True
    return False


def do_globbing(archive: dmftar.archive.Archive, args: List[str]) -> List[str]:
    '''expand wildcards in args list'''

    expanded = []
    for arg in args:
        debug(f'arg == {arg}')
        if is_pattern(arg):
            expanded.extend(glob(archive, arg))
        else:
            expanded.append(arg)

    debug(f'return {expanded}')
    return expanded


def do_pipe(pipe_cmd: str) -> None:
    '''execute pipe command with our output as input
    May raise ExecError
    '''

    global OUTPUT

    try:
        input_str = '\n'.join(OUTPUT) + '\n'
        cmd_arr = shlex.split(pipe_cmd)
        s_cmd = ' '.join(cmd_arr)
        debug(f'running pipe_cmd: {s_cmd}')
        subprocess.run(cmd_arr, shell=False, input=input_str, universal_newlines=True, check=True)

    except FileNotFoundError as err:
        raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

    except OSError as err:
        if err.errno == errno.EPIPE:
            # Broken pipe
            # this is not an interesting error; ignore
            pass
        else:
            error(f'dmftar pipe error: {err.strerror}')

    except subprocess.CalledProcessError as err:
        debug(f'pipe command {err.cmd[0]} exited with returncode {err.returncode}')

    OUTPUT = []


def prompt_path() -> str:
    '''Returns cwd as displayed in the prompt'''

    # strip off the root path
    assert ROOT is not None
    root_path = ROOT.fullpath + '/'
    root_len = len(root_path)
    assert CWD is not None
    path = CWD.fullpath + '/'
    if path[:root_len] == root_path:
        path = path[root_len:]
    return path


def shell(archive: dmftar.archive.Archive) -> None:
    '''do interactive shell
    May raise RuntimeError, OSError, ValueError, SilentError
    '''

    global TERM_WIDTH, REDIRECTED

    # first find out how many volumes or index files there are
    archive.count_volumes()

    if archive.remote is not None:
        raise RuntimeError('can not work with remote volumes, sorry')

    open_archive(archive)

    readline.set_completer(Completer(archive).complete)
    readline.set_completer_delims(' ')
    readline.parse_and_bind('tab: complete')

    TERM_WIDTH, _height = terminal_size()

    line = None     # type: Optional[str]
    errored = False

    while True:
        try:
            REDIRECTED = False

            prompt = f'\ndmftar {archive.name}:{prompt_path()}\n> '
            try:
                line = input(prompt)
            except EOFError:
                # user hit Ctrl-D
                line = None

            if line is None:
                print()
                break

            line = line.strip()
            if not line:
                continue

            # support for piping output through external command
            pipe_cmd = None
            try:
                if '|' in line:
                    line, pipe_cmd = split_pipe(line)
                    debug(f'line:[{line}]  pipe_cmd:[{pipe_cmd}]')
                    REDIRECTED = True

                argv = shlex.split(line)
            except ValueError as err:
                error(str(err))
                continue

            cmd = argv[0]
            # expand wildcard arguments
            args = do_globbing(archive, argv[1:])
            debug(f'args == {args}')

            if cmd in ('exit', 'quit'):
                break

            if cmd in ('?', 'help'):
                cmd_help()

            elif cmd == 'ls':
                TERM_WIDTH, _height = terminal_size()
                cmd_ls(archive, args)

            elif cmd == 'cd':
                cmd_cd(archive, args)

            elif cmd == '..':
                cmd_cd(archive, ['..', ])

            elif cmd == 'pwd':
                cmd_pwd()

            elif cmd == 'add':
                cmd_add(archive, args)

            elif cmd == 'remove':
                cmd_remove(archive, args)

            elif cmd == 'show':
                TERM_WIDTH, _height = terminal_size()
                cmd_show(args)

            elif cmd == 'extract':
                try:
                    cmd_extract(archive, args)
                except ExecError as err:
                    # print error message
                    error(str(err))
                    # if extract fails, ultimately give an error
                    # when quitting interactive mode
                    errored = True

            else:
                error(f'{cmd}: unknown command')
                pipe_cmd = None

            if pipe_cmd is not None:
                try:
                    do_pipe(pipe_cmd)
                except ExecError as err:
                    # print error message and continue
                    error(str(err))
            else:
                flush()

        except ValueError as err:
            error(str(err))
            errored = True
        except OSError as err:
            error(f'{err.filename}: {err.strerror}')
            errored = True

    if errored:
        # there were errors while extracting (message already printed)
        raise SilentError

# EOB
