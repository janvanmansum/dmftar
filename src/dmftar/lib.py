#
#   dmftar env.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''generic functions that didn't fit elsewhere'''

import os
import sys
import errno
import re
import shlex

from typing import Dict, List, Union, IO, Generator

# matches "${cmd}" syntax
REGEX_EXPAND_CMD = re.compile(r'\$\{([a-zA-Z0-9_+-]+)\}')


class SilentError(Exception):
    '''used when there was an error, but no message needs to be printed
    because messages were already printed
    '''



class ExecError(Exception):
    '''used for: failed to run command:'''



def read_nul_strings(f: IO) -> Generator[str, None, None]:
    '''yields nul terminated strings, read from input file f
    May raise OSError
    '''

    buf = ''
    while True:
        data = f.read(4096)
        if not data:
            break

        buf += data.decode()
        lines = buf.split('\x00')

        # take off last part, and keep it around
        buf = lines.pop()

        for line in lines:
            if line:
                yield line

    if buf:
        yield buf


def make_cmd_arr(cmd: str, symbols: Dict[str, Union[str, List[str]]]) -> List[str]:
    '''make array with command + arguments for subprocess calls
    Symbols is a dict like: symbols[var] => value
    Substitutes value in place of variable
    Returns the array
    '''

    # subst symbols in command line
    cmd_arr = []
    for part in cmd.split():
        if part and part[0] == '$' and part in symbols:
            value = symbols[part]
            assert value is not None
            if not value:
                continue

            if isinstance(value, list):
                cmd_arr.extend(value)
            else:
                cmd_arr.append(value)

        else:
            cmd_arr.append(part)

    return cmd_arr


def shlex_join(list_value: List[str]) -> str:
    '''join stringlist with quotes (as needed)
    Returns string
    '''

    # this is like what shlex.join() does
    # shlex.join() first appeared in Python 3.8
    s = ''
    for elem in list_value:
        if s:
            s += ' '
        s += shlex.quote(elem)
    return s


def mkdir_p(dirname: str, mode: int = 0o750) -> None:
    '''make directory, like mkdir -p
    May raise OSError, RuntimeError
    '''

    if os.path.isdir(dirname):
        # already exists
        # it's OK
        return

    # path already exists as file?
    if os.path.exists(dirname):
        raise OSError(errno.ENOTDIR, 'Not a directory', dirname)

    # make dir + leading dirs
    try:
        os.makedirs(dirname, mode)
    except OSError as err:
        raise RuntimeError(f'failed to make directory {dirname}: {err.strerror}') from err


def search_path(cmd: str) -> str:
    '''search the PATH for the location of cmd
    Returns fullpath
    Raises ValueError if cmd not found
    '''

    try:
        # maybe a full path was given
        path, _ = os.path.split(cmd)
        if path and os.path.isfile(cmd) and os.access(cmd, os.X_OK):
            return cmd

        # search the PATH environment variable
        # (this may raise KeyError)
        env_path = os.environ['PATH']
        for path in env_path.split(os.pathsep):
            fullpath = os.path.join(path, cmd)
            # check that the command is an executable file
            if os.path.isfile(fullpath) and os.access(fullpath, os.X_OK):
                return fullpath

    except (OSError, KeyError):
        # fallthrough to raise ValueError
        pass
    raise ValueError(f'{cmd}: command not found on PATH')


def expand_cmd(cmd_str: str) -> str:
    '''expand command variable(s) in command string to fullpath
    by searching the PATH for the command
    Returns substituted string
    '''

    while True:
        m = REGEX_EXPAND_CMD.search(cmd_str)
        if m is None:
            break

        cmd = m.groups()[0]
        fullpath = search_path(cmd)
        cmd_var = f'${{{cmd}}}'
        cmd_str = cmd_str.replace(cmd_var, fullpath)

    return cmd_str


def cat_file(filename: str) -> None:
    '''display file contents
    May raise OSError
    '''

    if not os.path.isfile(filename):
        raise OSError(errno.ENOENT, 'No such file', filename)

    with open(filename, 'rb') as f:
        while True:
            data = f.read(256 * 1024)
            if not data:
                break

            s = data.decode()
            sys.stdout.write(s)

    sys.stdout.flush()

# EOB
