#
#   dmftar main.py   WJ119
#   Copyright 2019 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''dmftar (client) main program implementation

tar directories on DMF dmapi filesystem
It uses GNU tar and its multiple-volumes feature
'''

#
#   create:
#   - create batches of filelists per volume in temp files
#   - invoke tar --multivolume -F volume-changer-script
#   - volume-changer issues dmget filelist (for next volume)
#   - volume-changer creates index file, checksum file
#   - volume-changer issues dmput previously created volume
#
#   list|extract:
#   - invoke tar --multivolume -F volume-changer-script
#   - volume-changer issues dmget next volume
#

import sys
import argparse
import subprocess

from typing import List, Optional

from dmftar import stdio, bytesize, checksum, interactive, remote, verify_content
from dmftar.stdio import warning, error, debug
from dmftar.lib import ExecError, SilentError
from dmftar.archive import Archive
from dmftar.argsparser import ArgsParser
from dmftar.config import Config

# set program name
PROG = stdio.PROG = 'dmftar'


class OptionError(Exception):
    '''error related to command-line options'''



class Args:
    '''represents program arguments'''

    def __init__(self) -> None:
        '''initialise instance'''

        self.tar_subcommand: Optional[str] = None
        self.remote: Optional[str] = None
        self.archive_dir: Optional[str] = None
        self.patterns: List[str] = []
        self.force_local = False
        self.opt_null = False
        self.regen_index = False
        self.regen_checksum = False

    def get_options(self, conf: Config) -> None:
        '''parse command-line arguments

        May raise OptionError
        '''

        default_volume_size = bytesize.sprint(conf.volume_size)

        epilog_text = 'Available checksum algorithms are:\n'
        # print list of available checksumming algorithms
        for algo in sorted(checksum.algorithms()):
            epilog_text += ' ' + algo

        parser = ArgsParser(prog=PROG, usage=f'''{PROG} -c       [options] -f [[USER@]REMOTE:]DEST/ [FILE|DIR ..]
or:    {PROG} -x       [options] -f [[USER@]REMOTE:]DIR/ [PATTERN ..]
or:    {PROG} -t|-d|-V [options] -f [[USER@]REMOTE:]DIR/
or:    {PROG} -i                 -f DIR/''', epilog=epilog_text, add_help=False)
        parser.add_argument('-h', '--help', action='store_true', help='Display this information')
        parser.add_argument('-c', '--create', action='store_true', help='Create a new archive')
        parser.add_argument('-t', '--list', action='store_true', help='List archive contents')
        parser.add_argument('-x', '--extract', action='store_true', help='Extract archive')
        parser.add_argument('-d', '--download', action='store_true', help='Download remote archive')
        parser.add_argument('-V', '--verify', action='store_true', help='Verify volume checksums')
        parser.add_argument('--verify-content', action='store_true', help='Compare archive index with directory entries')
        parser.add_argument('-i', '--interactive', action='store_true', help='Interactively browse and extract local archive')
        parser.add_argument('--regen-index', action='store_true', help='Re-create the index files')
        parser.add_argument('--regen-checksum', action='store_true', help='Re-create volume checksums')
        parser.add_argument('--delete-archive', action='store_true', help='Delete archive (no questions asked)')
        parser.add_argument('-f', '--archive', metavar='DIR', help='Set local or remote archive directory')

        parser.add_title('options:')
        parser.add_argument('--force-local', action='store_true', help='Archive dir is local even if it has a colon')
        parser.add_argument('-L', '--tape-length', metavar='SIZE', help=f"Set volume size (default: '{default_volume_size}')")
        parser.add_argument('-o', '--options', metavar='OPTIONS', help="Pass extra options to 'tar'")
        parser.add_argument('--tar', metavar='CMD', help=f"Use this (GNU) tar command (default: {conf.tar_binary})")
        parser.add_argument('--checksum', metavar='CHECKSUM', help=f"Set checksum algorithm (default: {conf.algorithm})")
        parser.add_argument('--no-checksum', action='store_true', help='Skip all checksumming')
        parser.add_argument('--keep-cache', action='store_true', help='Do not delete any downloaded volumes')
        parser.add_argument('--conf', metavar='CONFIGFILE', help=f"Change config file (default: {Config.DEFAULT_CONFIG})")
        parser.add_argument('--files-from', metavar='FILE', help='Extract members listed in file')
        parser.add_argument('--files-from0', metavar='FILE', help='Extract null-terminated members listed in file')
        parser.add_argument('-v', '--verbose', action='store_true', help='Show verbose output')
        parser.add_argument('-q', '--quiet', action='store_true', help='Suppress informational messages')
        parser.add_argument('--debug', action='store_true', help='Show debug messages')
        parser.add_argument('--version', action='store_true', help='Display version number and exit')

        parser.add_argument('args', nargs=argparse.REMAINDER, metavar='FILE|DIR|PATTERN ...')

        opts = parser.parse_args()

        if opts.debug:
            stdio.DEBUG = True

        if opts.help:
            parser.print_help()
            # exit with return code 1
            # which is something that default argparse does not do
            sys.exit(1)

        if opts.version:
            # pylint: disable=import-outside-toplevel
            import dmftar
            print(f'{PROG} version {dmftar.__version__}')
            print('Copyright 2014 SURFsara B.V.')
            print('Copyright 2021 SURF bv')
            sys.exit(0)

        if opts.verbose:
            conf.set('verbose', True)

        if opts.quiet:
            conf.set('quiet', True)

        if opts.debug:
            conf.set('debug', True)

        # load the config file
        cfg_filename = opts.conf
        conf.load(cfg_filename, hard_fail=cfg_filename is not None)

        # parse all other options
        if opts.create:
            if self.tar_subcommand is None:
                self.tar_subcommand = '-c'
            elif self.tar_subcommand != '-c':
                raise OptionError(f"options '--create' and '{self.tar_subcommand}' can not be combined")

        if opts.list:
            if self.tar_subcommand is None:
                self.tar_subcommand = '-t'
            elif self.tar_subcommand != '-t':
                raise OptionError(f"options '--list' and '{self.tar_subcommand}' can not be combined")

        if opts.extract:
            if self.tar_subcommand is None:
                self.tar_subcommand = '-x'
            elif self.tar_subcommand != '-x':
                raise OptionError(f"options '--extract' and '{self.tar_subcommand}' can not be combined")

        if opts.download:
            if self.tar_subcommand is None:
                self.tar_subcommand = '--download'
            elif self.tar_subcommand != '--download':
                raise OptionError(f"options '--download' and '{self.tar_subcommand}' can not be combined")

        if opts.verify:
            if self.tar_subcommand is None:
                self.tar_subcommand = '--verify'
            elif self.tar_subcommand != '--verify':
                raise OptionError(f"options '--verify' and '{self.tar_subcommand}' can not be combined")

        if opts.verify_content:
            if self.tar_subcommand is None:
                self.tar_subcommand = '--verify-content'
            elif self.tar_subcommand != '--verify-content':
                raise OptionError(f"options '--verify-content' and '{self.tar_subcommand}' can not be combined")

        if opts.interactive:
            if self.tar_subcommand is None:
                self.tar_subcommand = '--interactive'
            elif self.tar_subcommand != '--interactive':
                raise OptionError(f"options '--interactive' and '{self.tar_subcommand}' can not be combined")

        if opts.regen_index:
            # we need to pass this to the volume-changer
            # so also use conf.set()
            conf.set('regen_index', True)

            self.regen_index = True
            if self.tar_subcommand is None:
                self.tar_subcommand = '--regen'
            elif self.tar_subcommand != '--regen':
                raise OptionError(f"options '--regen-index' and '{self.tar_subcommand}' can not be combined")

        if opts.regen_checksum:
            # we need to pass this to the volume-changer
            # so also use conf.set()
            conf.set('regen_checksum', True)

            self.regen_checksum = True
            if self.tar_subcommand is None:
                self.tar_subcommand = '--regen'
            elif self.tar_subcommand != '--regen':
                raise OptionError(f"options '--regen-checksum' and '{self.tar_subcommand}' can not be combined")

        if opts.delete_archive:
            if '--delete-archive' not in sys.argv:
                raise OptionError("You must type '--delete-archive' out full "
                                  "if you wish to delete an archive")

            if self.tar_subcommand is None:
                self.tar_subcommand = '--delete-archive'
            elif self.tar_subcommand != '--delete-archive':
                raise OptionError(f"options '--delete-archive' and '{self.tar_subcommand}' can not be combined")

        self.archive_dir = opts.archive

        if opts.force_local:
            conf.set('force_local', True)
            self.force_local = True

        if opts.options:
            conf.set('tar_options', opts.options)

        if opts.tape_length:
            conf.set('volume_size', opts.tape_length)

        if opts.tar:
            conf.set('tar_binary', opts.tar)

        if opts.checksum:
            conf.set('algorithm', opts.checksum)

        if opts.no_checksum:
            conf.set('checksumming', False)

        if opts.keep_cache:
            conf.set('keep_cache', True)

        if opts.files_from:
            conf.from_file = opts.files_from

        if opts.files_from0:
            conf.from_file = opts.files_from0
            conf.opt_null = True

        if opts.verbose:
            conf.set('verbose', True)

        if opts.quiet:
            conf.set('quiet', True)

        if opts.debug:
            conf.set('debug', True)

        # check that an action was given
        if self.tar_subcommand is None:
            #raise OptionError('you must select an action, such as '
            #                  '--create, --list, --extract')
            parser.print_usage()
            sys.exit(1)

        if self.archive_dir is None:
            raise OptionError("no archive directory given (use option '-f')")

        args = opts.args
        self.check_args(args, parser)

        if self.tar_subcommand == '--verify' and not conf.checksumming:
            warning('checksumming was disabled, auto-enabling it')
            conf.checksumming = True

        if self.regen_checksum and conf.checksumming:
            # disable checksumming during staging
            conf.checksumming = False

        if conf.from_file and self.tar_subcommand != '-x':
            warning('option --from-file only works for --extract')

        if self.remote is not None:
            # require cmd_rsh and cmd_rcp
            if conf.cmd_rsh is None:
                raise ValueError('the configured rsh command was not found on PATH')
            #else:
            remote.init_cmd_rsh(conf.cmd_rsh)

            if conf.cmd_rcp is None:
                raise ValueError('the configured rcp command was not found on PATH')
            #else:
            remote.init_cmd_rcp(conf.cmd_rcp)

            # check that rsh to remote address is working
            remote.test_rsh(self.remote)

        # 'propagate' variables
        conf.remote = self.remote
        conf.archive_dir = self.archive_dir

    def check_args(self, args: List[str], parser: argparse.ArgumentParser) -> None:
        '''check command-line arguments
        May raise ValueError
        '''

        # args is only meaningful for --create and --extract (for now)
        if args and self.tar_subcommand in ('-t', '--download',
                                            '--verify',
                                            '--verify-content',
                                            '--interactive',
                                            '--regen',
                                            '--delete-archive'):
            raise OptionError('too many command-line arguments given')

        if self.tar_subcommand == '-c':
            if not args:
                parser.print_usage()
                sys.exit(1)

            self.patterns = args
            s_patterns = ' '.join(self.patterns)
            debug(f'args.patterns = {s_patterns}')

        elif self.tar_subcommand == '-x':
            self.patterns = args
            s_patterns = ' '.join(self.patterns)
            debug(f'args.patterns = {s_patterns}')

        # check given patterns
        for pattern in self.patterns:
            if pattern[0] == '-':
                raise ValueError("pattern can not start with a hyphen '-' character")

        # get remote address from self.archive_dir
        # if force-local, then do not assume that ':' is a remote archive
        assert self.archive_dir is not None     # this helps mypy
        if not self.force_local and ':' in self.archive_dir:
            remote_addr, remote_path = self.archive_dir.split(':', 1)
            if not remote_addr:
                raise ValueError('invalid remote address')

            if not remote_path:
                raise ValueError('remote path is missing')

            # It can be "USER@REMOTE", but no need to break that up
            # any further
            self.remote = remote_addr
            debug('args.remote = ' + self.remote)

            self.archive_dir = remote_path
        debug(f'args.archive_dir = {self.archive_dir}')

        # check out the given arguments
        if self.archive_dir[0] == '-':
            raise ValueError("path can not start with a hyphen '-' character")


def require_gnu_tar(tar_program: str) -> None:
    '''verify that we are using GNU tar
    May raise ExecError, RuntimeError
    '''

    debug(f'running {tar_program} --version')
    sys.stdout.flush()
    try:
        result = subprocess.run([tar_program, '--version'], shell=False,
                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                universal_newlines=True, check=True)
        output = result.stdout
        for line in output.split('\n'):
            if 'GNU ' in line:
                return

    except OSError as err:
        raise ExecError(f'{tar_program}: {err.strerror}') from err

    except subprocess.CalledProcessError as err:
        debug(f'{tar_program} exited with returncode {err.returncode}')

    raise RuntimeError(f"{stdio.PROG} requires GNU tar. Pass option '--tar=CMD' to change tar command")


def scoped_main() -> None:
    '''main program'''

    conf = Config()
    args = Args()
    args.get_options(conf)

    try:
        # make archive object
        arch = Archive(conf)

        # put certain settings in environment for volume-changer
        # (must be called after making an Archive ...)
        conf.putenv()

        # do action
        if args.tar_subcommand == '-c':
            # check that we have GNU tar
            require_gnu_tar(conf.tar_binary)
            arch.create(args.patterns)
        elif args.tar_subcommand == '-t':
            arch.do_list()
        elif args.tar_subcommand == '-x':
            require_gnu_tar(conf.tar_binary)
            arch.extract(args.patterns)
        elif args.tar_subcommand == '--download':
            arch.download()
        elif args.tar_subcommand == '--verify':
            if not arch.verify():
                # messages already printed
                raise SilentError
        elif args.tar_subcommand == '--verify-content':
            if not verify_content.verify_content(arch):
                # messages already printed
                raise SilentError
        elif args.tar_subcommand == '--interactive':
            require_gnu_tar(conf.tar_binary)
            interactive.shell(arch)
        elif args.tar_subcommand == '--regen':
            if conf.regen_index:
                require_gnu_tar(conf.tar_binary)
            arch.regenerate()
        elif args.tar_subcommand == '--delete-archive':
            arch.delete()

    except KeyboardInterrupt:   # user hit Ctrl-C
        print()
        sys.exit(127)
    finally:
        # delete temp file (if any)
        conf.cleanup()


def run() -> None:
    '''main program, really'''

    if not sys.stdout.isatty():
        # turn off debug color codes
        stdio.DEBUG_COLORS = False

    try:
        scoped_main()
    except (RuntimeError, ValueError, checksum.ChecksumError, remote.RemoteError, ExecError) as _err:
        error(str(_err))
        sys.exit(-1)

    except OSError as _err:
        if hasattr(_err, 'filename') and _err.filename is not None:
            error(f'{_err.filename}: {_err.strerror}')
        else:
            error(_err.strerror)
        sys.exit(-1)

    except OptionError as _err:
        error(str(_err))
        sys.exit(1)

    except SilentError:
        sys.exit(-1)

    except KeyboardInterrupt:   # user hit Ctrl-C
        print()
        sys.exit(127)

# EOB
