#
#   dmftar tar_env.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements class TarEnv, for TAR_xxx environment vars'''

import os
import errno

from typing import Optional

from dmftar.stdio import error, debug


class TarEnv:
    '''represents TAR_xxx environment variables'''

    VARS = ('TAR_SUBCOMMAND', 'TAR_ARCHIVE', 'TAR_VOLUME', 'TAR_FD')

    def __init__(self, subcommand: Optional[str] = None, archive: Optional[str] = None, volume: int = 1, fd: int = -1):
        '''initialise instance'''

        self.tar_subcommand = subcommand
        self.tar_archive = archive
        self.tar_volume = volume
        self.tar_fd = fd

    @staticmethod
    def check() -> None:
        '''only checks if the environment vars are present at all
        May raise EnvironmentError
        '''

        err_msgs = []
        for var in TarEnv.VARS:
            try:
                if not os.environ[var]:
                    err_msgs.append(f'environment variable {var} is empty')

            except KeyError:
                err_msgs.append(f'environment variable {var} not set')

        if err_msgs:
            last_msg = err_msgs.pop()
            # print all error messages
            for msg in err_msgs:
                error(msg)
            raise EnvironmentError(errno.EINVAL, last_msg)

    def getenv(self) -> None:
        '''get variables from environment
        Requirement: the environment _must_ have been set up
        May raise EnvironmentError
        '''

        try:
            self.tar_subcommand = os.environ['TAR_SUBCOMMAND']
        except KeyError as err:
            raise EnvironmentError(errno.EINVAL, 'environment variable TAR_SUBCOMMAND is not set') from err
        else:
            debug(f'TAR_SUBCOMMAND = {self.tar_subcommand}')
            if self.tar_subcommand not in ('-c', '-t', '-x', '--download',
                                           '--verify', '--regen',
                                           '--interactive'):
                raise EnvironmentError(errno.EINVAL, 'invalid value in environment var TAR_SUBCOMMAND')

        try:
            self.tar_archive = os.environ['TAR_ARCHIVE']
        except KeyError as err:
            raise EnvironmentError(errno.EINVAL, 'environment variable TAR_ARCHIVE is not set') from err
        else:
            debug(f'TAR_ARCHIVE = {self.tar_archive}')

        try:
            self.tar_volume = int(os.environ['TAR_VOLUME'])
        except KeyError as err:
            raise EnvironmentError(errno.EINVAL, 'environment variable TAR_VOLUME is not set') from err
        except ValueError as err:
            raise EnvironmentError(errno.EINVAL, 'invalid value in environment var TAR_VOLUME') from err
        else:
            debug(f'TAR_VOLUME = {self.tar_volume}')

        try:
            self.tar_fd = int(os.environ['TAR_FD'])
        except KeyError as err:
            raise EnvironmentError(errno.EINVAL, 'environment variable TAR_FD is not set') from err
        except ValueError as err:
            raise EnvironmentError(errno.EINVAL, 'invalid value in environment var TAR_FD') from err
        else:
            debug(f'TAR_FD = {self.tar_fd}')

    def putenv(self) -> None:
        '''put variables into environment'''

        assert self.tar_subcommand is not None
        assert self.tar_archive is not None
        os.environ['TAR_SUBCOMMAND'] = self.tar_subcommand
        os.environ['TAR_ARCHIVE'] = self.tar_archive
        os.environ['TAR_VOLUME'] = f'{self.tar_volume}'
        os.environ['TAR_FD'] = f'{self.tar_fd}'

# EOB
