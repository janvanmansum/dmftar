#
#   dmftar verify_content.py  WJ117
#   Copyright 2017 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''check archive content against filesystem directory content'''

import os
import stat
import time
import datetime
import pwd
import grp

from typing import Dict, Set

from dmftar.stdio import error, warning, stdout, debug
from dmftar.index import IndexEntry, is_gnu_fileparts

# only used for typing:
import dmftar.archive

# caches for uid/gid -> owner/group
UID_CACHE: Dict[int, str] = {}
GID_CACHE: Dict[int, str] = {}


def verify_content(arch: dmftar.archive.Archive) -> bool:
    '''Returns True when the archive content matches
    the filesystem directory content

    May raise ValueError, OSError when the index is broken
    However when the filesystem does not match (a good) index, return False
    '''

    # first find out how many volumes or index files there are
    arch.count_volumes()

    if arch.remote is not None:
        raise RuntimeError('can not work with remote volumes, sorry')

    verbose = arch.config.verbose

    errs = 0

    curr_set: Set[IndexEntry] = set([])
    dirstack = [curr_set, ]
    dirlevel = 1
    first_volume = True

    volnum = 1
    while volnum <= arch.num_volumes:
        stdout(f'verifying content for index #{volnum}')
        vol = arch.volume_bynumber(volnum)
        index_filename = f'{vol}.idx'
        debug('index_filename == ' + index_filename)
        try:
            with open(index_filename, encoding='utf-8') as f:
                lineno = 0
                for line in f:
                    lineno += 1

                    if not line:
                        continue

                    if not first_volume and lineno == 1 and line[0] == 'M':
                        # continued multi-volume header line
                        # the 'M------' volume header bits is now considered
                        # to be old-style GNU tar header
                        debug('continued old-style multi-volume header')
                        continue

                    entry = IndexEntry()
                    try:
                        entry.parse(line, init_unescape=True, init_mode=True)
                    except ValueError as err:
                        raise ValueError(f'{index_filename}:{lineno}: {err}') from err

                    if first_volume and lineno == 1 and entry.isdir():
                        first_volume = False
                        # the toplevel is a special case;
                        # stop altogether if it doesn't exist
                        # in the filesystem
                        if not compare_entry(entry):
                            return False

                        continue

                    if (not first_volume and lineno == 1
                            and entry.isfile() and is_gnu_fileparts(entry.path)):
                        # continued multi-volume (new style GNU tar header)
                        # may have a continued file with a "virtual" subdir
                        # named 'GNUFileParts.<pid>'
                        debug('continued new-style multi-volume header')
                        continue

                    entry_level = entry.path.count('/') + 1     # len(entry.path.split(os.sep))
                    if entry_level == dirlevel:
                        pass

                    elif entry_level > dirlevel:
                        # "recurse" into directory
                        while entry_level > dirlevel:
                            dirstack.append(set([]))
                            dirlevel += 1
                        curr_set = dirstack[-1]

                    elif entry_level < dirlevel:
                        # leave directory
                        while entry_level < dirlevel:
                            curr_set = dirstack.pop()
                            errs += compare_directory(curr_set, verbose)
                            dirlevel -= 1
                        curr_set = dirstack[-1]

#                    debug(f'<{dirlevel}> {entry}  {entry.path}')

                    curr_set.add(entry)

        except OSError as err:
            raise OSError(err.errno, err.strerror, index_filename) from err

        volnum += 1

    # it is possible there are still dirs on the stack
    # process them, too
    while dirstack:
        curr_set = dirstack.pop()
        errs += compare_directory(curr_set, verbose)

    if not errs:
        stdout('OK')

    # return True on success
    return errs == 0


def compare_directory(idx_set: Set[IndexEntry], verbose: bool = False) -> int:
    '''compare directory content with set of IndexEntry instances
    Returns number of mismatches
    and returns 0 if no issues found
    '''

    if not idx_set:
        return 0

    errs = 0
    idx_names = {x.name for x in idx_set}

    # get (any) first entry
    entry = None
    for entry in idx_set:
        break

    assert entry is not None

    path = os.path.dirname(entry.path)
    if not path:
        return errs

    if verbose:
        stdout(f'verifying {path}/')

    try:
        files = os.listdir(path)
    except OSError as err:
        error(f'{path}: {err.strerror}')
        errs += 1
    else:
        for name in files:
            if name not in idx_names:
                fullpath = os.path.join(path, name)
                error(f'{fullpath}: not present in archive')
                errs += 1

    for entry in idx_set:
        if not compare_entry(entry):
            errs += 1

    return errs


def compare_entry(entry: IndexEntry) -> bool:
    '''check an IndexEntry instance against filesystem
    Returns True if OK (but mind that it may report a warning)
    '''

    # pylint: disable=too-many-return-statements

    try:
        statbuf = os.lstat(entry.path)
    except OSError as err:
        error(f'{entry.path}: {err.strerror}')
        return False

    # check statbuf differences
    # first check file type
    # skip this check for hard links because we don't know the type
    s_ifmt = stat.S_IFMT(statbuf.st_mode)
    if entry.ifmt != s_ifmt and not entry.ishardlink():
        error(f'{entry.path}: file type has changed')
        return False

    # if !dir and do not follow symlinks
    if stat.S_ISLNK(entry.ifmt) or not stat.S_ISDIR(entry.ifmt):
        # check size, but do not check for links because
        # GNU tar index will always report size == 0 for symlinks
        # and hard links
        if (entry.size != statbuf.st_size
                and not stat.S_ISLNK(entry.ifmt) and not entry.ishardlink()):
            if statbuf.st_size > entry.size:
                error(f'{entry.path}: size mismatch: file is larger')
            else:
                error(f'{entry.path}: size mismatch: file is smaller')
            return False

        # look at mtime difference
        # tar index does not display seconds ...
        # so, one minute resolution
        entry_dt = datetime.datetime.fromtimestamp(time.mktime(entry.mtime))
        statbuf_dt = datetime.datetime.fromtimestamp(statbuf.st_mtime)
        delta = statbuf_dt - entry_dt
#        delta_seconds = delta.total_seconds()      # doesn't work in Python 2.6
        delta_seconds = delta.days * 86400 + delta.seconds
        if delta_seconds >= 60:
            warning(f'{entry.path}: file is newer')

    s_imode = stat.S_IMODE(statbuf.st_mode)
    if entry.imode != s_imode:
        warning(f'{entry.path}: mode has changed from {entry.imode:04o} to {s_imode:04o}')

    owner = owner_for_uid(statbuf.st_uid)
    num_owner = f'{statbuf.st_uid}'
    if entry.owner not in (owner, num_owner):
        warning(f'{entry.path}: owner has changed from {entry.owner} to {owner}')

    group = group_for_gid(statbuf.st_gid)
    num_group = f'{statbuf.st_gid}'
    if entry.group not in (group, num_group):
        warning(f'{entry.path}: group has changed from {entry.group} to {group}')

    # compare linkdest if symlink
    if entry.islink() and entry.linkdest is not None:
        try:
            linkdest = os.readlink(entry.path)
        except OSError as err:
            error(f'failed to read symbolic link {entry.path}: {err.strerror}')
            return False

        # be lenient about any trailing slashes
        linkdest_slash = linkdest + '/'
        if entry.linkdest not in (linkdest, linkdest_slash):
            error(f'{entry.path}: symbolic link destination has changed from {entry.linkdest} to {linkdest}')
            return False

    if entry.ishardlink() and entry.linkdest is not None:
        # check that hardlink destination exists
        try:
            _ = os.lstat(entry.linkdest)
        except OSError as err:
            error(f'{entry.path}: hard link destination {entry.linkdest}: {err.strerror}')
            return False

    return True


def owner_for_uid(uid: int) -> str:
    '''Returns owner (string) for numeric uid'''

    if uid in UID_CACHE:
        return UID_CACHE[uid]

    try:
        entry = pwd.getpwuid(uid)
        owner = entry.pw_name
    except KeyError:
        owner = f'{uid}'

    UID_CACHE[uid] = owner
    return owner


def group_for_gid(gid: int) -> str:
    '''Returns group (string) for numeric gid'''

    if gid in GID_CACHE:
        return GID_CACHE[gid]

    try:
        entry = grp.getgrgid(gid)
        group = entry.gr_name
    except KeyError:
        group = f'{gid}'

    GID_CACHE[gid] = group
    return group


# EOB
