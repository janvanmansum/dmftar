#
#   dmftar volume.py  WJ114
#   Copyright 2014 SURFsara
#   Copyright 2021 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements classes Volume and RemoteVolume'''

import os
import sys
import stat
import subprocess
import errno

from typing import Optional

from dmftar.stdio import stdout, warning, error, debug
from dmftar.lib import ExecError
from dmftar.checksum import Checksum, ChecksumError
from dmftar.filesystem import dmf_capable
from dmftar import remote

# 'Archive' type : problems with circular imports
#import dmftar.archive.Archive


class Volume:
    '''represents a single volume in an archive'''

    def __init__(self, arch) -> None:
        '''initialise instance'''

        self.archive = arch
        self.number = 1

    def __str__(self) -> str:
        '''Returns string object: volume filename'''

        assert self.archive.name is not None    # this helps mypy
        if self.number <= 1:
            return os.path.join(str(self.archive), '0000',
                                self.archive.name + '.tar')

        return os.path.join(str(self.archive), f'{self.number // 1000:04}',
                            f'{self.archive.name}.tar-{self.number}')

    def mkdir(self) -> None:
        '''make directory where to put this volume
        May raise OSError, RuntimeError
        '''

        dstdir = os.path.dirname(str(self))
        try:
            os.makedirs(dstdir, 0o750)
        except OSError as err:
            if err.errno == errno.EEXIST:
                # already exists (which is fine)
                return

            raise RuntimeError(f'failed to create dir {dstdir}: {err.strerror}') from err

    def getsize(self) -> int:
        '''Return size of this volume
        May raise OSError
        '''

        filename = str(self)
        return os.path.getsize(filename)

    def stage(self, missing_checksum_is_error: bool = False) -> None:
        '''get files here
        May raise ValueError, ChecksumError, OSError
        '''

        # prefetch: dmget next bunch of volumes
        self.prefetch()

        self.verify_chksum(missing_checksum_is_error)

    def store(self) -> None:
        '''put files on server
        May raise OSError, ValueError
        '''

        self.make_readonly()
        self._create_index()
        self._create_chksum()
        self.dmput()

    def dmget(self) -> None:
        '''get files from tape
        May raise OSError
        '''

        self._dmputget(self.archive.config.cmd_dmget)

    def dmput(self) -> None:
        '''put files to tape
        May raise OSError
        '''

        self._dmputget(self.archive.config.cmd_dmput)

    def _dmputget(self, cmd_putget: Optional[str]) -> None:
        '''dmput or dmget a volume + index file
        May raise OSError
        '''

        if cmd_putget is None:
            # no dmput/dmget command found on this system
            return

        filename = str(self)
        if not os.path.isfile(filename):
            raise OSError(errno.ENOENT, 'No such file', filename)

        # is filename on a DMF filesystem?
        if not dmf_capable(filename):
            # can not use dmput/dmget here
            return

        cmd_arr = cmd_putget.split()
        cmd_arr.append(filename)

        if not os.access(cmd_arr[0], os.X_OK):
            # dmget/dmput command not found
            # pretend it's OK; do not issue an error here
            debug(f'{cmd_arr[0]}: command not found')
            return

        # the index file too
        filename_idx = filename + '.idx'
        if os.path.isfile(filename_idx):
            cmd_arr.append(filename_idx)

        # NB. the checksum file is too small to be migrated by DMF

        s_cmd_arr = ' '.join(cmd_arr)
        debug(f'running {s_cmd_arr}')
        try:
            # return code of dmget/dmput is ignored
            subprocess.call(cmd_arr, shell=False)
        except OSError as err:
            raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

    def dmget_idx(self) -> None:
        '''dmget only the volume index
        This is used by do_list() to prefetch next index file
        May raise OSError
        '''

        volume_idx = f'{self}.idx'
        if not os.path.isfile(volume_idx):
            raise OSError(errno.ENOENT, 'No such file', volume_idx)

        if self.archive.config.cmd_dmget is None:
            # no dmget command on this system
            return

        # is volume_idx on a DMF filesystem?
        if not dmf_capable(volume_idx):
            # can not use dmget here
            return

        cmd_arr = self.archive.config.cmd_dmget.split()
        cmd_arr.append(volume_idx)

        if not os.access(cmd_arr[0], os.X_OK):
            # dmget/dmput command not found
            # pretend it's OK; do not raise an error
            debug(f'{cmd_arr[0]}: command not found')
            return

        s_cmd_arr = ' '.join(cmd_arr)
        debug(f'running {s_cmd_arr}')
        try:
            # return code of dmget/dmput is ignored
            subprocess.call(cmd_arr, shell=False)
        except OSError as err:
            raise ExecError(f'failed to run {cmd_arr[0]}: {err.strerror}') from err

    def prefetch(self) -> None:
        '''issue dmget to fetch bunch of volumes'''

        if self.number <= self.archive.num_volumes:
            volume_batch = self.archive.make_volume_batch(self.number)
            self.archive.dmget_volume_batch(volume_batch)

    def cleanup_cache(self) -> None:
        '''delete caches'''

        # this is a no-op for local volumes
        #pass

    def _create_index(self) -> None:
        '''create an index file for volume
        May raise OSError, ValueError, ExecError
        '''

        volume_filename = str(self)
        index_filename = volume_filename + '.idx'
        debug(f'create index: {index_filename}')
        try:
            with open(index_filename, 'w+b') as f:
                stdout(f'indexing volume #{self.number}')

                cmd_arr = [self.archive.config.tar_binary, '-t', '-v', '-f', volume_filename]
                if self.archive.config.force_local:
                    cmd_arr.append('--force-local')
                debug('running ' + ' '.join(cmd_arr))
                sys.stdout.flush()
                try:
                    # redirect output of tar tvf into the index file
                    # redirect stderr to /dev/null; we ignore errors
                    # because tar always prints errors for multi-volumes
                    subprocess.call(cmd_arr, shell=False, stdout=f, stderr=subprocess.DEVNULL)
                except OSError as err:
                    raise ExecError(f'failed to run {cmd_arr[0]} tvf: {err.strerror}') from err

        except OSError as err:
            raise OSError(err.errno, f'write error: {err.strerror}', index_filename) from err

        self._make_index_readonly()

    def _create_chksum(self) -> None:
        '''create a checksum file for volume
        May raise OSError, ValueError
        '''

        if (not self.archive.config.checksumming
                and not self.archive.config.regen_checksum):
            # skip checksumming because cmd-line option --no-chksum
            return

        stdout(f'checksumming volume #{self.number}')

        chksum = Checksum(str(self), self.archive.config.algorithm)
        chksum.calc()
        chksum.save()
        self._make_checksum_readonly()

    def verify_chksum(self, missing_is_error: bool = False) -> None:
        '''verify volume data against .chksum file
        May raise OSError, ValueError, ChecksumError
        '''

        if not self.archive.config.checksumming:
            # skip checksumming because cmd-line option --no-chksum
            return

        stdout(f'verifying checksum for volume #{self.number}')

        chksum1 = Checksum(str(self))
        try:
            chksum1.load()
        except (OSError, ValueError) as err:
            if missing_is_error:
                raise err
            #else
            # print the error message
            if isinstance(err, OSError):
                error(f'{err.filename}: {err.strerror}')    # pylint: disable=no-member
            else:
                error(str(err))

            warning(f'unable to verify checksum: {self}')
            # ignore error; return here
            return

        chksum2 = Checksum(str(self), chksum1.algorithm)
        chksum2.calc()
        if chksum1 != chksum2:
            raise ChecksumError(f'checksum failure: {self}')

    def make_readonly(self) -> None:
        '''remove write access bit'''

        filename = str(self)
        try:
            statbuf = os.lstat(filename)
            mode = stat.S_IMODE(statbuf.st_mode)
            # clear write access bits
            mode &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
            debug(f'chmod {mode:04o} {filename}')
            os.chmod(filename, mode)
        except OSError as err:
            warning(f'failed to make volume #{self.number} read-only: {err.strerror}')

    def _make_index_readonly(self) -> None:
        '''remove write access bit from index file'''

        filename = f'{self}.idx'
        try:
            statbuf = os.lstat(filename)
            mode = stat.S_IMODE(statbuf.st_mode)
            # clear write access bits
            mode &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
            debug(f'chmod {mode:04o} {filename}')
            os.chmod(filename, mode)
        except OSError as err:
            warning(f'failed to make volume index #{self.number} read-only: {err.strerror}')

    def _make_checksum_readonly(self) -> None:
        '''remove write access bit from checksum file'''

        filename = f'{self}.chksum'
        try:
            statbuf = os.lstat(filename)
            mode = stat.S_IMODE(statbuf.st_mode)
            # clear write access bits
            mode &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
            debug(f'chmod {mode:04o} {filename}')
            os.chmod(filename, mode)
        except OSError as err:
            warning(f'failed to make volume checksum #{self.number} read-only: {err.strerror}')

    def recreate_index(self) -> None:
        '''re-create the volume index file'''

        self._make_index_writeable()
        self._create_index()

    def _make_index_writeable(self) -> None:
        '''set write access bit on index file
        (so that it can be rewritten, regenerated)
        '''

        filename = f'{self}.idx'
        try:
            statbuf = os.lstat(filename)
            mode = stat.S_IMODE(statbuf.st_mode)
            # set user write access bit
            mode |= stat.S_IWUSR
            debug(f'chmod {mode:04o} {filename}')
            os.chmod(filename, mode)
        except OSError as err:
            # silently ignored (unless debug mode)
            debug(f'failed to set write access bit on volume index #{self.number} : {err.strerror}')

    def recreate_chksum(self) -> None:
        '''re-create the volume checksum file'''

        self._make_checksum_writeable()
        self._create_chksum()

    def _make_checksum_writeable(self) -> None:
        '''set write access bit on checksum file
        (so that it can be rewritten, regenerated)
        '''

        filename = f'{self}.chksum'
        try:
            statbuf = os.lstat(filename)
            mode = stat.S_IMODE(statbuf.st_mode)
            # set user write access bit
            mode |= stat.S_IWUSR
            debug(f'chmod {mode:04o} {filename}')
            os.chmod(filename, mode)
        except OSError as err:
            # silently ignored (unless debug mode)
            debug(f'failed to set write access bit on volume checksum #{self.number} : {err.strerror}')

    def lock_directory(self) -> None:
        '''sets the directory as read-only'''

        path = os.path.dirname(str(self))
        try:
            statbuf = os.lstat(path)
        except OSError:
            # stat failed
            # most likely ENOENT no such directory
            # or EACCES permission denied
            # silently ignored
            return

        mode = stat.S_IMODE(statbuf.st_mode)
        if mode & (stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH) != 0:
            # clear write access bits
            mode &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
            try:
                debug(f'chmod {mode:04o} {path}')
                os.chmod(path, mode)
            except OSError as err:
                warning(f'failed to set directory {path} read-only: {err.strerror}')

    def unlock_directory(self) -> None:
        '''sets the directory as writeable (by user)'''

        path = os.path.dirname(str(self))
        try:
            statbuf = os.lstat(path)
        except OSError:
            # stat failed
            # most likely ENOENT no such directory
            # or EACCES permission denied
            # silently ignored
            return

        mode = stat.S_IMODE(statbuf.st_mode)
        if mode & stat.S_IWUSR == 0:
            # set user write access bit
            mode |= stat.S_IWUSR
            try:
                debug(f'chmod {mode:04o} {path}')
                os.chmod(path, mode)
            except OSError as err:
                # should this be an error?
                warning(f'failed to set directory {path} writeable: {err.strerror}')



class RemoteVolume(Volume):
    '''represents a remote volume in a remote archive
    RemoteVolume inherits from Volume, and overrides stage()/store()
    so that it copies volume files from and to the remote server
    '''

    def __init__(self, arch) -> None:
        '''initialise instance'''

        super().__init__(arch)

        self.cached = False

    def __str__(self) -> str:
        '''Returns path+filename of volume in local cache'''

        assert self.archive.name is not None    # this helps mypy

        if self.number <= 1:
            return os.path.join(self.archive.cache_dir, '0000',
                                self.archive.name + '.tar')

        return os.path.join(self.archive.cache_dir, f'{self.number // 1000:04}',
                            f'{self.archive.name}.tar-{self.number}')

    def cache_filename(self) -> str:
        '''Returns path+filename of volume in local cache'''

        return str(self)

    def remote_filename(self) -> str:
        '''Returns remote path+filename
        (but without leading user@server part)
        '''

        # same as str(Volume), but use remote_filename() for clarity

        return super().__str__()

    def mkdir(self) -> None:
        '''make cache directory where to put this volume
        May raise RuntimeError, OSError
        '''

        dstdir = os.path.dirname(self.cache_filename())
        try:
            os.makedirs(dstdir, 0o750)
        except OSError as err:
            if err.errno == errno.EEXIST:
                # already exists, ignore
                return

            raise RuntimeError(f'failed to create dir {dstdir}: {err.strerror}') from err

    def getsize(self) -> int:
        '''Return size of this volume
        May raise RemoteError, ValueError
        '''

        output = remote.command(self.archive.remote, f'size {self.remote_filename()}')
        assert output is not None
        try:
            return int(output)
        except ValueError as err:
            raise ValueError(f'invalid server response: {output}') from err

    def stage(self, missing_checksum_is_error: bool = False) -> None:
        '''get files here
        May raise OSError, ValueError, ChecksumError, RemoteError
        '''

        # issue remote pre-stage of list of volumes
        self.prefetch()
        # copy file to here
        self.remote_copy_stage()
        self.verify_chksum(missing_checksum_is_error)

    def store(self) -> None:
        '''put files on server
        May raise OSError, ValueError, RemoteError
        '''

        self.make_readonly()
        self._create_index()
        self._create_chksum()
        # copy files to server
        self.remote_copy_store()
        # issue remote dmput
        self.dmput()

    def dmget(self) -> None:
        '''issue remote dmget command and
        copy the file over to local cache
        May raise OSError, RemoteError
        '''

        # issue remote dmget
        remote.command(self.archive.remote, f'dmget {self.remote_filename()}')

    def dmput(self) -> None:
        '''copy the file over to remote server and
        issue remote dmput command
        May raise OSError, RemoteError
        '''

        # issue remote dmput
        remote.command(self.archive.remote, f'dmput {self.remote_filename()}')

    def dmget_idx(self) -> None:
        '''dmget only the volume index'''

        # this is a no-op for remote volumes
        # There is no purpose in issueing a remote dmget just before
        # a remote index command. The server will prefetch any further
        # index files 'locally' on the remote side
        #pass

    def prefetch(self) -> None:
        '''issue remote prefetch to dmget next bunch of volumes'''

        # calc number of volumes to stage
        try:
            # integer divide with rounding up
            # usually volbatch_size: 1 TB // volume_size: 10 GB
            # which gives: num == 100
            num = (self.archive.config.volbatch_size + (self.archive.config.volume_size - 1)) // self.archive.config.volume_size

            # num should not be less than 1
            num = max(1, num)
        except ZeroDivisionError:
            # just do something; try stage next 10 volumes
            num = 10

        if self.number > 1 and self.number % num != 0:
            # only issue in multiples of 'num'
            # do not issue intermediate gets
            return

        start_number = self.number
        if start_number + num > self.archive.num_volumes:
            num = self.archive.num_volumes - start_number + 1
            if num <= 0:
                # more no volumes to get
                return

        # make argument string
        num_plus_archivedir = f'{start_number}+{num}+{self.archive}'
        # return value is ignored (don't care)
        try:
            remote.command(self.archive.remote, 'prefetch ' + num_plus_archivedir)
        except remote.RemoteError:
            pass

    def remote_copy_stage(self) -> None:
        '''run remote-copy-cmd to copy remote files to here
        May raise RuntimeError, OSError, RemoteError
        '''

        filename = self.remote_filename()
        filelist = [filename, filename + '.idx']
        if self.archive.config.checksumming:
            filelist.append(filename + '.chksum')

        dstdir = os.path.dirname(self.cache_filename())
        try:
            os.makedirs(dstdir, 0o750)
        except OSError as err:
            if err.errno != errno.EEXIST:
                raise RuntimeError(f'failed to create dir {dstdir}: {err.strerror}') from err

        assert self.archive.remote is not None  # this helps mypy

        remote.copy_stage(self.archive.remote, filelist, dstdir)

        # this volume is now cached
        self.cached = True

    def remote_copy_store(self) -> None:
        '''copy volume, index, and checksum files to remote server
        May raise OSError, RemoteError
        '''

        filename = self.cache_filename()
        filelist = []
        if not os.path.isfile(filename):
            raise OSError(errno.ENOENT, 'No such file', filename)

        filelist.append(filename)

        if os.path.isfile(filename + '.idx'):
            filelist.append(filename + '.idx')

        if os.path.isfile(filename + '.chksum'):
            filelist.append(filename + '.chksum')

        assert self.archive.remote is not None  # this helps mypy

        dstdir = os.path.dirname(self.remote_filename())
        # remote mkdir -p
        # because the remote destination dir has to exist
        remote.command(self.archive.remote, 'mkdir ' + dstdir)
        remote.copy_store(filelist, self.archive.remote, dstdir)
        # remote dmput will be issued later

        # on success, cleanup local cache
        self.cleanup_cache()

    def verify_chksum(self, missing_is_error: bool = False) -> None:
        '''verify volume data against .chksum file
        May raise OSError, ValueError, ChecksumError
        '''

        # can only work on cache
        assert self.cached

        if not self.archive.config.checksumming:
            # skip checksumming because cmd-line option --no-chksum
            return

        stdout(f'verifying checksum for volume #{self.number}')

        # use the cached file
        filename = self.cache_filename()

        chksum1 = Checksum(filename)
        try:
            chksum1.load()
        except (OSError, ValueError) as err:
            if missing_is_error:
                raise err
            #else:
            # print the error message
            if isinstance(err, OSError):
                error(f'{err.filename}: {err.strerror}')    # pylint: disable=no-member
            else:
                error(str(err))

            warning(f'unable to verify checksum: {self}')
            # ignore error; return here
            return

        chksum2 = Checksum(filename, chksum1.algorithm)
        chksum2.calc()
        if chksum1 != chksum2:
            raise ChecksumError(f'checksum failure: {self}')

    def cleanup_cache(self) -> None:
        '''delete local cache'''

        filename = self.cache_filename()

        if self.archive.config.keep_cache:
            debug(f'keep-cache, not deleting {filename}')
            return

        debug(f'cleanup cache: deleting {filename}')
        try:
            os.unlink(filename)
        except OSError:
            # silently ignore any errors
            pass

        filename_idx = filename + '.idx'
        debug(f'cleanup cache: deleting {filename_idx}')
        try:
            os.unlink(filename_idx)
        except OSError:
            # silently ignore any errors
            pass

        filename_chksum = filename + '.chksum'
        debug(f'cleanup cache: deleting {filename_chksum}')
        try:
            os.unlink(filename_chksum)
        except OSError:
            # silently ignore any errors
            pass

        # Note that the numbered subdir still exists
        # we may still need it;
        # it will finally be cleaned up by Archive.cleanup_cachedir()

    def lock_directory(self) -> None:
        '''locks remote directory'''

        path = os.path.dirname(self.remote_filename())
        try:
            remote.command(self.archive.remote, f'lock {path}')
        except remote.RemoteError:
            warning(f'failed to set remote directory {path} read-only')

    def unlock_directory(self) -> None:
        '''unlocks remote directory'''

        path = os.path.dirname(self.remote_filename())
        try:
            remote.command(self.archive.remote, f'unlock {path}')
        except remote.RemoteError:
            warning(f'failed to set write access on directory {path}')

# EOB
