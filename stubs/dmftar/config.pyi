from typing import List, Optional, Union

class Config:
    DEFAULT_CONFIG: str
    DEFAULT_RC: str
    KB: int
    MB: int
    GB: int
    TB: int
    DEFAULT_VOLUME_SIZE: int
    MINIMUM_SIZE: int
    MAXIMUM_SIZE: int
    DEFAULT_VOLBATCH_SIZE: int
    DEFAULT_TAR_BINARY: str
    DEFAULT_VOLUME_CHANGER: str
    DEFAULT_ALGORITHM: str
    DEFAULT_CMD_RSH: str
    DEFAULT_CMD_RCP: str
    DEFAULT_CMD_DMGET: str
    DEFAULT_CMD_DMPUT: str
    DEFAULT_PIPE_CMD_DMGET_BATCH: str
    DEFAULT_CMD_FIND_BATCH: str
    @staticmethod
    def find_config() -> None: ...
    verbose: bool
    quiet: bool
    debug: bool
    tar_options: str
    volume_size: int
    volbatch_size: int
    checksumming: bool
    algorithm: str
    tar_binary: str
    volume_changer: str
    cmd_find_batch: str
    cmd_rsh: Optional[str]
    cmd_rcp: Optional[str]
    cmd_dmget: Optional[str]
    cmd_dmput: Optional[str]
    pipe_cmd_dmget_batch: Optional[str]
    force_local: bool
    keep_cache: bool
    from_file: Optional[str]
    opt_null: bool
    regen_index: bool
    regen_checksum: bool
    volbatch_start: int
    remote: Optional[str]
    archive_dir: Optional[str]
    cache_dir: Optional[str]
    tmpfile: Optional[str]
    def __init__(self) -> None: ...
    def load(self, filename: Optional[str] = ..., hard_fail: bool = ..., pure: bool = ...) -> None: ...
    def load_params(self, filename: str) -> None: ...
    def set_value(self, var: str, val: Union[str, bool], pure: bool = ...) -> None: ...
    def set(self, var: str, val: Union[str, bool, List[str]], pure: bool = ...) -> None: ...
    @staticmethod
    def set_boolean(var: str, value: Union[str, bool, List[str]]) -> bool: ...
    @staticmethod
    def set_stringlist(_var: str, value: Union[str, bool, List[str]]) -> List[str]: ...
    @staticmethod
    def error(filename: Optional[str], lineno: int, msg: str) -> None: ...
    def getenv(self) -> None: ...
    def putenv(self) -> None: ...
    def cleanup(self) -> None: ...
