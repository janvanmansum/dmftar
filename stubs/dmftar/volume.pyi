from dmftar.archive import Archive

class Volume:
    archive: Archive
    number: int
    def __init__(self, arch) -> None: ...
    def mkdir(self) -> None: ...
    def getsize(self) -> int: ...
    def stage(self, missing_checksum_is_error: bool = ...) -> None: ...
    def store(self) -> None: ...
    def dmget(self) -> None: ...
    def dmput(self) -> None: ...
    def dmget_idx(self) -> None: ...
    def prefetch(self) -> None: ...
    def cleanup_cache(self) -> None: ...
    def create_index(self) -> None: ...
    def create_chksum(self) -> None: ...
    def verify_chksum(self, missing_is_error: bool = ...) -> None: ...
    def make_readonly(self) -> None: ...
    def make_index_readonly(self) -> None: ...
    def make_checksum_readonly(self) -> None: ...
    def recreate_index(self) -> None: ...
    def make_index_writeable(self) -> None: ...
    def recreate_chksum(self) -> None: ...
    def make_checksum_writeable(self) -> None: ...
    def lock_directory(self) -> None: ...
    def unlock_directory(self) -> None: ...

class RemoteVolume(Volume):
    cached: bool
    def __init__(self, arch) -> None: ...
    def cache_filename(self) -> str: ...
    def remote_filename(self) -> str: ...
    def mkdir(self) -> None: ...
    def getsize(self) -> int: ...
    def stage(self, missing_checksum_is_error: bool = ...) -> None: ...
    def store(self) -> None: ...
    def dmget(self) -> None: ...
    def dmput(self) -> None: ...
    def dmget_idx(self) -> None: ...
    def prefetch(self) -> None: ...
    def remote_copy_stage(self) -> None: ...
    def remote_copy_store(self) -> None: ...
    def verify_chksum(self, missing_is_error: bool = ...) -> None: ...
    def cleanup_cache(self) -> None: ...
    def lock_directory(self) -> None: ...
    def unlock_directory(self) -> None: ...
