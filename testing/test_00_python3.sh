#! /bin/bash
#
#	test_python3.sh
#

PYTHON=$(command -v python3)

if [[ -z $PYTHON ]]
then
	echo "fail: python3 command not found"
	exit 1
fi

#
#	dmftar tests the version by itself, but let's also do it here
#
python3 -c "
import sys
if sys.version_info < (3, 6):
    sys.exit(1)
"

if [ $? -ne 0 ]
then
	echo "fail: python 3.6 or higher is required"
	exit 1
fi

exit 0

# EOB
