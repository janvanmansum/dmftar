#! /bin/bash
#
#	test_help.sh
#

$DMFTAR --help >$OUTPUT
if [ $? -ne 1 ]
then
	echo "fail: invalid exit status"
	exit 1
fi

split_debug_output $OUTPUT

LINES=`wc -l $OUTPUT |awk '{ print $1 }'`
if [ $LINES -lt 6 ]
then
	echo "fail: output too short"
	exit 1
fi

HAVE_USAGE=`grep ^usage: $OUTPUT`
if [ -z "$HAVE_USAGE" ]
then
	echo "fail: missing usage line"
	exit 1
fi

HAVE_PROGNAME=`grep dmftar $OUTPUT`
if [ -z "$HAVE_PROGNAME" ]
then
	echo "fail: missing progname"
	exit 1
fi

grep -q create $OUTPUT
if [ $? -ne 0 ]
then
	echo "fail: help does not list option 'create'"
	exit 1
fi

grep -q list $OUTPUT
if [ $? -ne 0 ]
then
	echo "fail: help does not list option 'list'"
	exit 1
fi

grep -q extract $OUTPUT
if [ $? -ne 0 ]
then
	echo "fail: help does not list option 'extract'"
	exit 1
fi

exit 0

# EOB
