#! /bin/bash
#
#	test_md5.sh
#

#OPENSSL_MD5=$(echo "hello, world" |openssl md5)
HEXDIGEST="22c3683b094136c3398391ae71b20f04"

python3 -c "import hashlib; m = hashlib.new('md5'); m.update('hello, world\n'.encode()); print(m.hexdigest())" >$OUTPUT 2>&1

PYTHON_MD5=$(cat $OUTPUT)

if [[ $PYTHON_MD5 != $HEXDIGEST ]]
then
	echo "fail: hexdigest mismatch"
	exit 1
fi

exit 0

# EOB
