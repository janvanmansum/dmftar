#! /bin/bash
#
#	test_verify_content.sh
#

if [ ! -d "${DATASET}.dmftar" ]
then
	echo "fail: no such directory: ${DATASET}.dmftar"
	exit 1
fi

$DMFTAR --verify-content -f ${DATASET}.dmftar >$OUTPUT 2>&1
RET=$?
if [ $RET -ne 0 ]
then
	cat $OUTPUT
	echo "fail: exit value $RET"
	exit 1
fi

split_debug_output $OUTPUT

grep -q error $OUTPUT
if [ $? -ne 1 ]
then
	cat $OUTPUT
	echo "fail: there were errors"
	exit 1
fi

# EOB
